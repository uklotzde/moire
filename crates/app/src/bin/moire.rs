use std::{
    fs,
    ops::Not as _,
    path::{Path, PathBuf},
    rc::Rc,
    thread,
    time::Duration,
};

use aoide::desktop_app::Handle;
use directories::ProjectDirs;
use rtrb::RingBuffer;

use hid_io::UsagePage;
use moire_audio_dsp::{engine as audio_engine, Engine as AudioEngine};
use moire_audio_io::jack::JackBackend;
use moire_ctrl::circular_fifo;
use moire_hid_io as hid_io;
use moire_ui::Gui;

// hack around https://github.com/Windfisch/rust-assert-no-alloc/issues/7
#[cfg(not(all(windows, target_env = "gnu")))]
#[global_allocator]
static A: assert_no_alloc::AllocDisabler = assert_no_alloc::AllocDisabler;

const GC_PERIOD: Duration = Duration::from_secs(1);

#[must_use]
pub fn app_name() -> &'static str {
    // The crate name differs from the desired app name, i.e. we cannot
    // use `env!("CARGO_PKG_NAME")` for this purpose.
    "moire"
}

#[must_use]
pub fn app_dirs() -> Option<ProjectDirs> {
    ProjectDirs::from("", "", app_name())
}

#[must_use]
fn init_config_dir(app_dirs: &ProjectDirs) -> &Path {
    let app_config_dir = app_dirs.config_dir();
    if let Err(err) = fs::create_dir_all(app_config_dir) {
        log::warn!(
            "Failed to create config directory '{dir}': {err}",
            dir = app_config_dir.display(),
        );
    }
    app_config_dir
}

fn app_config_dir() -> Option<PathBuf> {
    app_dirs()
        .as_ref()
        .map(init_config_dir)
        .map(Path::to_path_buf)
}

fn main() {
    env_logger::init();

    let config_dir = match app_config_dir() {
        Some(config_dir) => config_dir,
        None => {
            log::error!("Config directory is unavailable");
            return;
        }
    };
    if !config_dir.exists() {
        log::error!(
            "Config directory '{dir_path}' does not exist",
            dir_path = config_dir.display()
        );
        return;
    }
    let config_dir_readonly = match config_dir
        .metadata()
        .map(|metadata| metadata.permissions().readonly())
    {
        Ok(readonly) => readonly,
        Err(err) => {
            log::error!("Failed to query permissions of config directory: {err}");
            // Assume that the directory is writable, i.e. not read-only
            false
        }
    };
    if config_dir_readonly {
        log::warn!(
            "Config directory (read-only): {dir_path}",
            dir_path = config_dir.display()
        );
    } else {
        log::info!(
            "Config directory: {dir_path}",
            dir_path = config_dir.display()
        );
    }

    let tokio_rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(err) => {
            log::error!("Failed to create Tokio runtime: {err}");
            return;
        }
    };

    let aoide_initial_settings =
        match aoide::desktop_app::settings::State::restore_from_parent_dir(&config_dir) {
            Ok(settings) => settings,
            Err(err) => {
                log::error!("Failed to restore aoide settings: {err}");
                return;
            }
        };
    let aoide_db_config = match aoide_initial_settings.create_database_config() {
        Ok(db_config) => db_config,
        Err(err) => {
            log::error!("Failed to create aoide database config: {err}");
            return;
        }
    };
    log::debug!("Commissioning library: {aoide_db_config:?}");
    let aoide_handle = match Handle::commission(&aoide_db_config) {
        Ok(library_backend) => library_backend,
        Err(err) => {
            log::error!("Failed to commission library backend: {err}");
            return;
        }
    };

    let mut gc = basedrop::Collector::new();
    let gc_handle = gc.handle();
    thread::spawn(move || loop {
        gc.collect();
        thread::sleep(GC_PERIOD);
    });

    let (to_audio_tx, from_gui_rx) =
        circular_fifo::new_producer_consumer::<audio_engine::Command>();
    let (to_gui_tx, from_audio_rx) = RingBuffer::<audio_engine::Event>::new(2048);
    let audio_engine = AudioEngine::new(from_gui_rx, to_gui_tx);
    let initial_render_config = audio_engine.render_config().clone();
    let (jack_backend, jack_process_tx) =
        JackBackend::new(gc_handle.clone(), audio_engine, app_name());
    let jack_backend = Rc::new(jack_backend);

    // FIXME: Move out of the main module, only here for testing and demonstration purposes.
    let mut usbhid_api = match hid_io::Api::new() {
        Ok(api) => api,
        Err(err) => {
            log::error!("Failed to initialize HID subsystem: {err}");
            return;
        }
    };
    log::info!("Querying HID devices");
    let usbhid_devices = match usbhid_api.query_devices_dedup() {
        Ok(vec) => vec.into_iter().filter(|device| {
            matches!(
                UsagePage::from(device.info().usage_page()),
                UsagePage::VendorDefined(_)
            )
        }),
        Err(err) => {
            log::error!("Failed to query HID devices: {err}");
            return;
        }
    };

    let mut device_context = None;
    for mut device in usbhid_devices {
        let device_info = device.info().clone();
        log::info!(
            "Found HID device {manufacturer_name} {product_name}: path = {path}, vid = 0x{vid:0.4x}, pid = 0x{pid:0.4x}, sn = '{sn}', usage = {usage}, usage_page = {usage_page:?}, release_number = {release_number}, interface_number = {interface_number}",
            manufacturer_name = device_info.manufacturer_string().and_then(|s| s.trim().is_empty().not().then_some(s)).unwrap_or("(no manufacturer name)"),
            product_name = device_info.product_string().and_then(|s| s.trim().is_empty().not().then_some(s)).unwrap_or("(no product name)"),
            path = device_info.path().to_str().unwrap_or_default(),
            vid = device_info.vendor_id(),
            pid = device_info.product_id(),
            sn = device_info.serial_number().unwrap_or_default(),
            usage = device_info.usage(),
            usage_page = UsagePage::from(device_info.usage_page()),
            release_number = device_info.release_number(),
            interface_number = device_info.interface_number(),
        );
        if let Err(err) = device.connect(&usbhid_api) {
            log::error!("Failed to connect device {device_info:?}: {err}");
        } else if hid_io::ni_traktor_s4mk3::DeviceContext::is_supported(&device_info) {
            debug_assert!(
                device_context.is_none(),
                "only a single device is supported"
            );
            device_context = match hid_io::ni_traktor_s4mk3::DeviceContext::attach(device) {
                Ok(mut device_context) => {
                    log::info!(
                        "Initializing device: {device_info:?}",
                        device_info = device_context.info()
                    );
                    device_context.initialize();
                    Some(device_context)
                }
                Err(err) => {
                    log::error!("Failed to attach {device_info:?}: {err}");
                    None
                }
            }
        }
    }

    let gui = Gui::new(
        config_dir,
        tokio_rt.handle().clone(),
        aoide_handle,
        aoide_initial_settings,
        initial_render_config,
        to_audio_tx,
        from_audio_rx,
        jack_process_tx,
        Rc::clone(&jack_backend),
        gc_handle,
    );

    if let Err(err) = gui.run() {
        log::error!("Failed to run UI: {err}");
    }

    if let Some(mut device_context) = device_context {
        log::info!(
            "Finalizing device: {device_info:?}",
            device_info = device_context.info()
        );
        device_context.finalize();
    }
}
