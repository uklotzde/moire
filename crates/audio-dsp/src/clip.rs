use std::{ops::Range, time::Duration};

use creek::{ReadDiskStream, ReadStreamOptions, SeekMode, SymphoniaDecoder};
use rubato::{
    Resampler as _, SincFixedOut, SincInterpolationParameters, SincInterpolationType,
    WindowFunction,
};

use crate::{
    graph::RenderConfig, FrameSize, Frames, IFrames, ISampleRate, Sample, SampleRate, Seconds,
    SAMPLE_ZERO,
};

const MAX_CHANNELS: usize = 2;

const SEEK_MODE: SeekMode = SeekMode::Auto;

pub const DEFAULT_TEMPO_RATIO: f64 = 1.0; // +/-0%

// Lower bound for tempo ratio (negative values would play in reverse which can't be done with the tempo fader)
pub const MIN_TEMPO_RATIO: f64 = 0.01; // -99% (to prevent a division by zero or overflow by very small divisors)

// Upper bound for tempo ratio
pub const MAX_TEMPO_RATIO: f64 = 2.0; // +100%

#[derive(Debug, Clone, PartialEq)]
pub struct Props {
    pub timeline_start: Seconds,
    pub timeline_sample_rate: SampleRate,
    pub stream_range: Range<FrameSize>,
    pub stream_sample_rate: ISampleRate,
    // tempo_ratio: <1.0 = slower, >1.0 = faster
    pub tempo_ratio: f64,
}

impl Props {
    fn is_valid(&self) -> bool {
        let Self {
            timeline_start: _,
            timeline_sample_rate,
            stream_range: _,
            stream_sample_rate,
            tempo_ratio: _,
        } = self;
        *timeline_sample_rate > SampleRate::ZERO && *stream_sample_rate > ISampleRate::ZERO
    }

    /// Calculates the stream range projected onto the timeline,
    /// considering both differing sample rates and tempo ratio.
    fn timeline_range(&self) -> Range<Frames> {
        let stream_start = Frames::new(*self.stream_range.start as _);
        let stream_end = Frames::new(*self.stream_range.end as _);
        let stream_frames = stream_end - stream_start;
        let stream_time = stream_frames / SampleRate::from(self.stream_sample_rate);
        debug_assert!(self.tempo_ratio > 0.0);
        let play_time = stream_time / self.tempo_ratio;
        let timeline_frames = play_time * self.timeline_sample_rate;
        let timeline_start = self.timeline_start * self.timeline_sample_rate;
        let timeline_end = timeline_start + timeline_frames;
        debug_assert!(timeline_start <= timeline_end);
        timeline_start..timeline_end
    }
}

struct Playhead {
    stream: ReadDiskStream<SymphoniaDecoder>,
    resampler: SincFixedOut<Sample>,
}

const MAIN_PLAYHEAD_INDEX: usize = 0;
const PRELISTEN_PLAYHEAD_INDEX: usize = 1;

/// Clip handles one (segment of an) audio file within a deck.
pub struct Clip {
    props: Props,

    timeline_range: Range<Frames>,

    chunk_size: FrameSize,

    // Shared input buffer for both the main and prelisten resampler.
    resampler_input_buffer: Vec<Vec<Sample>>,

    // Temporary, intermediate process buffer for both the main and prelisten resampler.
    // Only needed for edge cases at the start or end of a clip when rendering chunks
    // with only a partial overlap.
    resampler_process_buffer: Vec<Vec<Sample>>,

    playheads: [Playhead; 2],
}

fn make_resampler(base_ratio: f64, chunk_size: FrameSize) -> SincFixedOut<Sample> {
    let params = SincInterpolationParameters {
        sinc_len: 256,
        f_cutoff: 0.95,
        oversampling_factor: 128,
        interpolation: SincInterpolationType::Cubic,
        window: WindowFunction::Hann2,
    };
    SincFixedOut::new(base_ratio, 1.0 / MIN_TEMPO_RATIO, params, *chunk_size, 2).unwrap()
}

fn make_resampler_default() -> SincFixedOut<Sample> {
    make_resampler(1.0, FrameSize::ZERO)
}

impl Clip {
    pub fn new(
        timeline_start: Seconds,
        file_path: &std::path::Path,
        file_offset: Seconds,
        file_duration: Option<Duration>,
    ) -> anyhow::Result<Clip> {
        let main_opts = ReadStreamOptions {
            num_cache_blocks: 20,
            ..Default::default()
        };
        let main_stream = ReadDiskStream::<SymphoniaDecoder>::new(file_path, 0, main_opts)?;

        let prelisten_opts = ReadStreamOptions {
            num_cache_blocks: 20,
            ..Default::default()
        };
        let prelisten_stream =
            ReadDiskStream::<SymphoniaDecoder>::new(file_path, 0, prelisten_opts)?;

        let file_info = main_stream.info();
        let stream_sample_rate = ISampleRate::from_hz(
            file_info
                .sample_rate
                .ok_or_else(|| anyhow::anyhow!("no sample rate"))?,
        );

        let stream_start = file_offset * SampleRate::from(stream_sample_rate);
        let stream_end_max = Frames::new(file_info.num_frames as _);
        let stream_end = if let Some(file_duration) = file_duration {
            let stream_end =
                stream_start + Seconds::from(file_duration) * SampleRate::from(stream_sample_rate);
            Frames::new(stream_end_max.min(*stream_end))
        } else {
            stream_end_max
        };
        debug_assert!(stream_start <= stream_end);
        let stream_start = FrameSize::new(*stream_start.floor_to_int() as _);
        let stream_end = FrameSize::new(*stream_end.ceil_to_int() as _);
        let stream_range = stream_start..stream_end;

        let props = Props {
            timeline_start,
            timeline_sample_rate: SampleRate::ZERO,
            stream_range,
            stream_sample_rate,
            tempo_ratio: DEFAULT_TEMPO_RATIO,
        };
        let timeline_range = props.timeline_range();

        let resampler_input_buffer = vec![vec![]; MAX_CHANNELS];
        let resampler_process_buffer = vec![vec![]; MAX_CHANNELS];

        let main_playhead = Playhead {
            stream: main_stream,
            resampler: make_resampler_default(),
        };
        let prelisten_playhead = Playhead {
            stream: prelisten_stream,
            resampler: make_resampler_default(),
        };

        Ok(Self {
            props,
            timeline_range,
            chunk_size: FrameSize::ZERO,
            resampler_process_buffer,
            resampler_input_buffer,
            playheads: [main_playhead, prelisten_playhead],
        })
    }

    pub fn props(&self) -> &Props {
        &self.props
    }

    pub fn set_tempo_ratio(&mut self, tempo_ratio: f64) -> f64 {
        let tempo_ratio = tempo_ratio.clamp(MIN_TEMPO_RATIO, MAX_TEMPO_RATIO);
        self.props.tempo_ratio = tempo_ratio;
        self.timeline_range = self.props.timeline_range();
        debug_assert!(tempo_ratio > 0.0);
        let resample_ratio_relative = 1.0 / tempo_ratio;
        for playhead in &mut self.playheads {
            playhead
                .resampler
                .set_resample_ratio_relative(resample_ratio_relative, true)
                .unwrap();
        }
        tempo_ratio
    }

    fn make_resamplers(&mut self) {
        debug_assert!(self.props.stream_sample_rate > ISampleRate::ZERO);
        if self.chunk_size > FrameSize::ZERO && self.props.timeline_sample_rate > SampleRate::ZERO {
            let base_ratio =
                self.props.timeline_sample_rate / SampleRate::from(self.props.stream_sample_rate);
            for playhead in &mut self.playheads {
                playhead.resampler = make_resampler(base_ratio, self.chunk_size);
            }
            let input_buf_size = self.playheads[MAIN_PLAYHEAD_INDEX]
                .resampler
                .input_frames_max();
            debug_assert_eq!(
                input_buf_size,
                self.playheads[PRELISTEN_PLAYHEAD_INDEX]
                    .resampler
                    .input_frames_max()
            );
            log::debug!("Resampler input buffer size = {input_buf_size}");
            for input_buf in &mut self.resampler_input_buffer {
                let additional_capacity =
                    input_buf.capacity().max(input_buf_size) - input_buf.capacity();
                input_buf.reserve(additional_capacity);
            }
            let process_buf_size = *self.chunk_size;
            log::debug!("Resampler process buffer size = {process_buf_size}");
            for process_buf in &mut self.resampler_process_buffer {
                let additional_capacity =
                    process_buf.capacity().max(process_buf_size) - process_buf.capacity();
                process_buf.reserve(additional_capacity);
                process_buf.resize(process_buf.capacity(), SAMPLE_ZERO);
            }
        } else {
            log::debug!(
                "Cannot make resamplers: {props:?}, chunk_size = {chunk_size}",
                props = self.props,
                chunk_size = self.chunk_size
            );
        }
    }

    /// Update the sample rate while already rendering.
    ///
    /// This method is real-time safe.
    pub fn update_timeline_sample_rate(&mut self, timeline_sample_rate: SampleRate) {
        if self.props.timeline_sample_rate == timeline_sample_rate {
            // Nothing to do
            return;
        }

        self.props.timeline_sample_rate = timeline_sample_rate;

        self.make_resamplers();
    }

    pub fn prepare_to_render(&mut self, render_config: &RenderConfig) {
        log::debug!("Preparing to render: {render_config:?}");
        let RenderConfig {
            sample_rate: timeline_sample_rate,
            samples_per_channel: chunk_size,
            channel_buffer_layout: _,
        } = render_config;
        if self.chunk_size == *chunk_size
            && self.props.timeline_sample_rate == *timeline_sample_rate
        {
            // Nothing to do
            log::debug!("Already prepared to render");
            return;
        }

        log::debug!("Updating rendering config");
        self.chunk_size = *chunk_size;
        self.props.timeline_sample_rate = *timeline_sample_rate;
        self.timeline_range = self.props.timeline_range();

        for playhead in &mut self.playheads {
            // Cache the start of the file into cache with index `0`.
            playhead
                .stream
                .cache(0, *self.props.stream_range.start)
                .unwrap();
            // Tell the streams to seek to the beginning of file. This will also alert the stream to the existence
            // of the cache with index `0`.
            playhead
                .stream
                .seek(*self.props.stream_range.start, SEEK_MODE)
                .unwrap();
        }

        // Wait until the buffers are filled before entering the real-time context.
        log::debug!("Blocking until main stream is ready");
        self.playheads[MAIN_PLAYHEAD_INDEX]
            .stream
            .block_until_ready()
            .unwrap();
        log::debug!("Blocking until prelisten stream is ready");
        self.playheads[PRELISTEN_PLAYHEAD_INDEX]
            .stream
            .block_until_ready()
            .unwrap();

        self.make_resamplers();
    }

    pub fn seek(&mut self, delta: Seconds) {
        self.props.timeline_start -= delta;
        self.timeline_range = self.props.timeline_range();

        let stream_delta =
            (delta * self.props.stream_sample_rate * self.props.tempo_ratio).round_to_int();

        for playhead in &mut self.playheads {
            let new_position: usize = (playhead.stream.playhead() as isize
                + *stream_delta as isize)
                .try_into()
                .unwrap_or(*self.props.stream_range.start)
                .max(*self.props.stream_range.start);
            playhead.stream.seek(new_position, SEEK_MODE).unwrap();
        }
    }

    pub fn process<ChannelBuf: AsMut<[Sample]>>(
        &mut self,
        main_timeline_playhead: IFrames,
        main_buffer: &mut [ChannelBuf],
        prelisten_timeline_playhead: IFrames,
        prelisten_buffer: &mut [ChannelBuf],
    ) -> bool {
        debug_assert!(self.props.is_valid());
        debug_assert!(self.chunk_size > FrameSize::ZERO);
        debug_assert_eq!(self.timeline_range, self.props.timeline_range());
        let main_chunk_start = Frames::new(*main_timeline_playhead as _);
        let main_chunk_end = main_chunk_start + Frames::new(*self.chunk_size as _);
        let main_chunk_range = main_chunk_start..main_chunk_end;
        let main_processed = process_inner(
            &self.timeline_range,
            main_chunk_range,
            main_buffer,
            &mut self.resampler_input_buffer,
            &mut self.resampler_process_buffer,
            &mut self.playheads[MAIN_PLAYHEAD_INDEX],
        );
        let prelisten_chunk_start = Frames::new(*prelisten_timeline_playhead as _);
        let prelisten_chunk_end = prelisten_chunk_start + Frames::new(*self.chunk_size as _);
        let prelisten_chunk_range = prelisten_chunk_start..prelisten_chunk_end;
        let prelisten_processed = process_inner(
            &self.timeline_range,
            prelisten_chunk_range,
            prelisten_buffer,
            &mut self.resampler_input_buffer,
            &mut self.resampler_process_buffer,
            &mut self.playheads[PRELISTEN_PLAYHEAD_INDEX],
        );
        main_processed || prelisten_processed
    }
}

#[allow(clippy::useless_asref)] // false positive?
fn process_inner<ChannelBuf: AsMut<[Sample]>>(
    timeline_range: &Range<Frames>,
    chunk_range: Range<Frames>,
    chunk_buf: &mut [ChannelBuf],
    resampler_input_buffer: &mut [Vec<Sample>],
    resampler_process_buffer: &mut [Vec<Sample>],
    playhead: &mut Playhead,
) -> bool {
    if chunk_range.end <= timeline_range.start || chunk_range.start >= timeline_range.end {
        // Empty intersection
        return false;
    }
    let chunk_size = chunk_range.end - chunk_range.start;
    debug_assert_eq!(*chunk_size, chunk_size.round());
    let chunk_size = chunk_size.to_int_unchecked().as_usize();
    let output_start = if chunk_range.start < timeline_range.start {
        // Unlikely edge case: Partial intersection -> fill with 0 samples
        let silence_size = (timeline_range.start - chunk_range.start)
            .round_to_int()
            .as_usize();
        debug_assert!(silence_size < chunk_size);
        let output_start = silence_size;
        for output_buf in chunk_buf.as_mut() {
            output_buf.as_mut()[..output_start].fill(SAMPLE_ZERO);
        }
        output_start
    } else {
        // Likely regular case
        0
    };
    let output_end = if chunk_range.end > timeline_range.end {
        // Unlikely edge case: Partial intersection -> fill with 0 samples
        let silence_size = (chunk_range.end - timeline_range.end)
            .round_to_int()
            .as_usize();
        debug_assert!(silence_size < chunk_size);
        let output_end = chunk_size - silence_size;
        for output_buf in chunk_buf.as_mut() {
            output_buf.as_mut()[output_end..].fill(SAMPLE_ZERO);
        }
        output_end
    } else {
        // Likely regular case
        chunk_size
    };
    debug_assert!(output_start < output_end);
    let output_size = output_end - output_start;
    debug_assert!(output_size > 0);
    debug_assert!(output_size <= chunk_size);
    let (second_channel_index, active_channels_mask) = match playhead.stream.info().num_channels {
        1 => (
            0,                              // mono (first channel) -> dual mono
            Some([true, false].as_slice()), // resample only the first channel
        ),
        2 => (
            1,    // 2nd stereo channel,
            None, // resample all channels
        ),
        _ => unreachable!("too many input channels"),
    };
    let input_len = playhead.resampler.input_frames_next();
    match playhead.stream.read(input_len) {
        Ok(read_data) => {
            for (channel_index, input_buf) in resampler_input_buffer[..=second_channel_index]
                .iter_mut()
                .enumerate()
            {
                let read_buf = read_data.read_channel(channel_index);
                // Usually, creek returns the number of frames requested and they can be
                // passed directly to rubato without a copy. However, at the end of the
                // file, creek returns less than the number of requested frames. In this case,
                // rubato still needs its specified amount of input frames, so copy the frames
                // from creek into an intermediate buffer.
                debug_assert!(read_buf.len() <= input_len);
                debug_assert!(input_buf.capacity() >= input_len);
                input_buf.clear();
                input_buf.extend_from_slice(read_buf);
                input_buf.extend(std::iter::repeat(SAMPLE_ZERO).take(input_len - read_buf.len()));
            }
            if output_start > 0 {
                // Unlikely edge case: Process into intermediate, temporary buffer and copy
                let (_, output_frames) = playhead
                    .resampler
                    .process_into_buffer(
                        resampler_input_buffer,
                        resampler_process_buffer,
                        active_channels_mask,
                    )
                    .unwrap();
                debug_assert!(output_frames >= output_size);
                debug_assert_eq!(2, chunk_buf.len());
                chunk_buf[0].as_mut()[output_start..output_end]
                    .copy_from_slice(&resampler_process_buffer[0][..output_size]);
                chunk_buf[1].as_mut()[output_start..output_end].copy_from_slice(
                    &resampler_process_buffer[second_channel_index][..output_size],
                );
            } else {
                // Likely regular case: Process directly into output buffer
                let (_, output_frames) = playhead
                    .resampler
                    .process_into_buffer(resampler_input_buffer, chunk_buf, active_channels_mask)
                    .unwrap();
                debug_assert!(output_frames <= chunk_size);
                // At the end of a clip the resampler might produce overshooting sample
                // frames that overwrite the previously zeroed range. This is fine.
                debug_assert!(output_frames >= output_size);
            }
        }
        Err(err) => {
            match err {
                // This could happen when seeking near the end of the file
                creek::read::ReadError::EndOfFile => {}
                _ => log::error!("Failed to read audio data: {err}"),
            }
            for output_buf in chunk_buf.as_mut() {
                output_buf.as_mut()[output_start..output_end].fill(SAMPLE_ZERO);
            }
        }
    }
    true
}
