use std::{borrow::Cow, sync::Arc};

use basedrop::Owned;

use moire_ctrl::{
    interpolation::{RampingF32, RampingMode, RampingProfile},
    param::{
        self,
        register::{DescriptorRegistration, RegisteredDescriptor, RegistrationHeader},
        Param, RegisterError, ValueDescriptor, ValueRangeDescriptor,
    },
};

use crate::{
    graph::{
        InitializeError, Node, NodeDescriptor, PrepareToRenderError, RenderConfig, RenderProps,
    },
    Clip, IFrames, Sample, SampleRate, Seconds, SAMPLE_ZERO,
};

/// Decks handle one audio track on the timeline. A Deck
/// can contain an arbitrary number of Clips.
pub struct Deck {
    index: u8,
    channels_per_stream: usize,

    node_descriptor: NodeDescriptor,
    render_config: Option<RenderConfig>,

    tempo_ratio: f32,

    main_playhead: IFrames,
    main_ramping_gain_ratio: RampingF32,

    prelisten_playhead_offset: IFrames,
    prelisten_ramping_gain_ratio: RampingF32,

    main_out_buffer: Vec<Vec<f32>>,

    clips: Option<Owned<Vec<Clip>>>,
}

const DEFAULT_GAIN_RATIO: f32 = 1.0;
const MIN_GAIN_RATIO: f32 = 0.0;

const DEFAULT_TEMPO_RATIO: f32 = super::clip::DEFAULT_TEMPO_RATIO as f32;
const MIN_TEMPO_RATIO: f32 = super::clip::MIN_TEMPO_RATIO as f32;
const MAX_TEMPO_RATIO: f32 = super::clip::MAX_TEMPO_RATIO as f32;

// These are the indices of the Params in each Decks' Vec<Param>,
// distinct from the globally unique moire_ctrl::param::register::Id in the registry.
const INPUT_PARAM_INDEX_GAIN: usize = 0;
const INPUT_PARAM_INDEX_TEMPO: usize = 1;
const INPUT_PARAM_INDEX_PRELISTEN: usize = 2;

const OUTPUT_PARAM_INDEX_PRELISTEN: usize = 0;

pub fn gain_address(address_prefix: &str) -> param::Address<'static> {
    param::Address::from(format!("{address_prefix}/gain"))
}

pub const fn gain_descriptor() -> param::Descriptor<'static> {
    param::Descriptor {
        direction: param::Direction::Input,
        name: Cow::Borrowed("gain"),
        unit: Some(Cow::Borrowed("ratio")),
        value: ValueDescriptor {
            default: param::Value::F32(DEFAULT_GAIN_RATIO),
            range: ValueRangeDescriptor {
                min: Some(param::Value::F32(MIN_GAIN_RATIO)),
                max: None,
            },
        },
    }
}

pub fn tempo_address(address_prefix: &str) -> param::Address<'static> {
    param::Address::from(format!("{address_prefix}/tempo"))
}

pub const fn tempo_descriptor() -> param::Descriptor<'static> {
    param::Descriptor {
        direction: param::Direction::Input,
        name: Cow::Borrowed("tempo"),
        unit: Some(Cow::Borrowed("ratio")),
        value: ValueDescriptor {
            default: param::Value::F32(DEFAULT_TEMPO_RATIO),
            range: ValueRangeDescriptor {
                min: Some(param::Value::F32(MIN_TEMPO_RATIO)),
                max: Some(param::Value::F32(MAX_TEMPO_RATIO)),
            },
        },
    }
}

pub fn prelisten_address(address_prefix: &str) -> param::Address<'static> {
    param::Address::from(format!("{address_prefix}/prelisten"))
}

pub const fn prelisten_descriptor(direction: param::Direction) -> param::Descriptor<'static> {
    param::Descriptor {
        direction,
        name: Cow::Borrowed("prelisten"),
        unit: None,
        value: ValueDescriptor {
            default: param::Value::Bool(false),
            range: ValueRangeDescriptor::unbounded(),
        },
    }
}

fn register_input_params(
    param_registry: &mut param::Registry,
    address_prefix: &str,
) -> Result<Vec<Param<'static>>, RegisterError> {
    debug_assert!(address_prefix.starts_with("/in/"));
    let gain_address = gain_address(address_prefix);
    let gain_descriptor = gain_descriptor();
    let DescriptorRegistration {
        header: RegistrationHeader { id: gain_id, .. },
        ..
    } = param_registry.register_descriptor(gain_address.clone(), gain_descriptor.clone())?;
    let gain_param = Param {
        address: gain_address,
        descriptor: gain_descriptor,
        id: gain_id,
    };
    let tempo_address = tempo_address(address_prefix);
    let tempo_descriptor = tempo_descriptor();
    let DescriptorRegistration {
        header: RegistrationHeader { id: tempo_id, .. },
        ..
    } = param_registry.register_descriptor(tempo_address.clone(), tempo_descriptor.clone())?;
    let tempo_param = Param {
        address: tempo_address,
        descriptor: tempo_descriptor,
        id: tempo_id,
    };
    let prelisten_address = prelisten_address(address_prefix);
    let prelisten_descriptor = prelisten_descriptor(param::Direction::Input);
    let DescriptorRegistration {
        header: RegistrationHeader {
            id: prelisten_id, ..
        },
        ..
    } = param_registry
        .register_descriptor(prelisten_address.clone(), prelisten_descriptor.clone())?;
    let prelisten_param = Param {
        address: prelisten_address,
        descriptor: prelisten_descriptor,
        id: prelisten_id,
    };
    Ok(vec![gain_param, tempo_param, prelisten_param])
}

fn register_output_params(
    param_registry: &mut param::Registry,
    address_prefix: &str,
) -> Result<Vec<(Param<'static>, param::atomic::SharedValue)>, RegisterError> {
    debug_assert!(address_prefix.starts_with("/out/"));
    let prelisten_address = prelisten_address(address_prefix);
    let prelisten_descriptor = prelisten_descriptor(param::Direction::Output);
    let DescriptorRegistration {
        header: RegistrationHeader {
            id: prelisten_id, ..
        },
        descriptor:
            RegisteredDescriptor {
                output_value: prelisten_value,
                ..
            },
    } = param_registry
        .register_descriptor(prelisten_address.clone(), prelisten_descriptor.clone())?;
    let prelisten_value = Arc::clone(prelisten_value.unwrap());
    let prelisten_param = Param {
        address: prelisten_address,
        descriptor: prelisten_descriptor,
        id: prelisten_id,
    };
    Ok(vec![(prelisten_param, prelisten_value)])
}

impl Deck {
    pub const NUMBER_OF_STREAMS: usize = 2; // main + prelisten

    /// Total number of channels from all streams
    pub const fn channels_out(channels_per_stream: usize) -> usize {
        Self::NUMBER_OF_STREAMS * channels_per_stream
    }

    pub fn param_address_prefix(direction: param::Direction, index: u8) -> String {
        let direction = match direction {
            param::Direction::Input => "in",
            param::Direction::Output => "out",
        };
        format!("/{direction}/deck/{index}")
    }

    pub fn new(index: u8, channels_per_stream: usize) -> Self {
        let main_out_buffer = vec![Vec::<Sample>::new(); channels_per_stream];
        Self {
            index,
            channels_per_stream,
            node_descriptor: Default::default(),
            render_config: None,
            tempo_ratio: Default::default(),
            main_playhead: Default::default(),
            main_ramping_gain_ratio: RampingF32::new(Default::default()),
            prelisten_playhead_offset: Default::default(),
            prelisten_ramping_gain_ratio: RampingF32::new(Default::default()),
            main_out_buffer,
            clips: None,
        }
    }

    pub fn node_descriptor(&self) -> &NodeDescriptor {
        &self.node_descriptor
    }

    fn reset_node_descriptor(&mut self, node_descriptor: NodeDescriptor) {
        self.node_descriptor = node_descriptor;
        self.main_ramping_gain_ratio = RampingF32::new(
            *self.node_descriptor.params_in[INPUT_PARAM_INDEX_GAIN]
                .descriptor
                .value
                .default
                .as_f32()
                .unwrap(),
        );
        self.prelisten_ramping_gain_ratio = RampingF32::new(
            if *self.node_descriptor.params_in[INPUT_PARAM_INDEX_PRELISTEN]
                .descriptor
                .value
                .default
                .as_bool()
                .unwrap()
            {
                1.0
            } else {
                0.0
            },
        );
        self.tempo_ratio = *self.node_descriptor.params_in[INPUT_PARAM_INDEX_TEMPO]
            .descriptor
            .value
            .default
            .as_f32()
            .unwrap();
    }

    /// Update the sample rate while already rendering.
    ///
    /// This method is real-time safe.
    pub fn update_sample_rate(&mut self, sample_rate: SampleRate) {
        if let Some(render_config) = &mut self.render_config {
            render_config.sample_rate = sample_rate;
            if let Some(clips) = &mut self.clips {
                for clip in clips.iter_mut() {
                    clip.update_timeline_sample_rate(sample_rate);
                }
            }
        }
    }

    pub fn load_clip(&mut self, new_clip: Clip, mut storage: Owned<Vec<Clip>>) {
        // storage has a capacity enough for the old clips plus new_clip without
        // allocating on push, so move the old clips into storage.
        if let Some(clips) = &mut self.clips {
            storage.extend(clips.drain(..));
        }
        storage.push(new_clip);
        storage.sort_unstable_by(|a, b| {
            a.props()
                .timeline_start
                .partial_cmp(&b.props().timeline_start)
                .unwrap()
        });
        self.clips = Some(storage);
    }

    pub fn seek_clip(&mut self, clip_index: usize, delta: Seconds) {
        if let Some(clip) = self.clips.as_mut().unwrap().get_mut(clip_index) {
            clip.seek(-delta);
        }
    }

    pub fn set_prelisten_playhead_offset(&mut self, prelisten_playhead_offset: IFrames) {
        self.prelisten_playhead_offset = prelisten_playhead_offset;
    }

    pub fn main_out_buffer(&self) -> &[Vec<Sample>] {
        &self.main_out_buffer
    }
}

fn apply_ramping_gain<ChannelBuf: AsMut<[Sample]>>(
    render_buf: &mut [ChannelBuf],
    ramping_gain: &mut RampingF32,
) {
    let sample_len = render_buf[0].as_mut().len();
    for channel_buf in render_buf.iter_mut() {
        debug_assert_eq!(channel_buf.as_mut().len(), sample_len);
        for (sample, gain) in channel_buf.as_mut().iter_mut().zip(ramping_gain.clone()) {
            *sample *= gain;
        }
    }
    ramping_gain.advance(sample_len);
}

impl Node<Sample> for Deck {
    fn initialize(
        &mut self,
        param_registry: &mut param::Registry,
    ) -> Result<NodeDescriptor, InitializeError> {
        let params_in = register_input_params(
            param_registry,
            &Self::param_address_prefix(param::Direction::Input, self.index),
        )?;
        let params_out = register_output_params(
            param_registry,
            &Self::param_address_prefix(param::Direction::Output, self.index),
        )?;
        let channels_in = 0;
        let channels_out = Self::channels_out(self.channels_per_stream);
        let node_descriptor = NodeDescriptor {
            params_in,
            params_out,
            channels_in,
            channels_out,
            ..Default::default()
        };
        self.reset_node_descriptor(node_descriptor.clone());
        log::debug!("Deck #{index}: {node_descriptor:?}", index = self.index);
        Ok(node_descriptor)
    }

    fn set_input_param_value(&mut self, index: usize, value: param::Value) {
        match index {
            INPUT_PARAM_INDEX_GAIN => {
                let target_value = MIN_GAIN_RATIO.max(*value.as_f32().unwrap());
                self.main_ramping_gain_ratio.reset(target_value);
            }
            INPUT_PARAM_INDEX_TEMPO => {
                let value = *value.as_f32().unwrap();
                let tempo_ratio = value.clamp(MIN_TEMPO_RATIO, MAX_TEMPO_RATIO);
                if self.tempo_ratio == tempo_ratio {
                    // Unchanged
                    return;
                }
                self.tempo_ratio = tempo_ratio;
                if let Some(clips) = &mut self.clips {
                    for clip in clips.iter_mut() {
                        clip.set_tempo_ratio(tempo_ratio as _);
                    }
                }
            }
            INPUT_PARAM_INDEX_PRELISTEN => {
                let value = *value.as_bool().unwrap();
                let target_value = if value { 1.0 } else { 0.0 };
                self.prelisten_ramping_gain_ratio.reset(target_value);
                // Update the value of the corresponding output parameter
                // that reflects the current state.
                let (_, output_value) =
                    &self.node_descriptor.params_out[OUTPUT_PARAM_INDEX_PRELISTEN];
                output_value.store_bool(value);
            }
            _ => unreachable!("param index out of range"),
        }
    }

    fn prepare_to_render(
        &mut self,
        render_config: &RenderConfig,
        first_frame: IFrames,
    ) -> Result<RenderProps, PrepareToRenderError> {
        self.main_playhead = first_frame;

        let buffer_size = *render_config.samples_per_channel;
        for channel in &mut *self.main_out_buffer {
            channel.resize(buffer_size, SAMPLE_ZERO);
        }

        // Default ramping profile: Linear during a single process buffer
        let ramping_profile = RampingProfile {
            mode: RampingMode::Linear,
            steps: buffer_size,
        };

        self.main_ramping_gain_ratio
            .reset_profile(self.main_ramping_gain_ratio.target_value(), ramping_profile);
        self.prelisten_ramping_gain_ratio.reset_profile(
            self.prelisten_ramping_gain_ratio.target_value(),
            ramping_profile,
        );

        if let Some(clips) = &mut self.clips {
            for clip in clips.iter_mut() {
                clip.prepare_to_render(render_config);
            }
        }

        self.render_config = Some(render_config.clone());
        Ok(Default::default())
    }

    fn render<ChannelBuf: AsMut<[Sample]>>(&mut self, render_buf: &mut [ChannelBuf]) {
        self.debug_assert_render_preconditions(
            &self.node_descriptor,
            self.render_config.as_ref().unwrap(),
            render_buf,
        );

        let mut any_clips_processed = false;
        if let Some(clips) = &mut self.clips {
            let (main_buf, prelisten_buf) = render_buf.split_at_mut(self.channels_per_stream);
            for clip in clips.iter_mut() {
                let clip_processed = clip.process(
                    self.main_playhead,
                    main_buf,
                    self.main_playhead + self.prelisten_playhead_offset,
                    prelisten_buf,
                );
                if clip_processed {
                    any_clips_processed = true;
                }
            }
        }
        if !any_clips_processed {
            for channel_buf in render_buf.iter_mut() {
                channel_buf.as_mut().fill(SAMPLE_ZERO);
            }
        }

        let (main_buf, prelisten_buf) = render_buf.split_at_mut(self.channels_per_stream);
        apply_ramping_gain(main_buf, &mut self.main_ramping_gain_ratio);
        apply_ramping_gain(prelisten_buf, &mut self.prelisten_ramping_gain_ratio);

        for (dst, src) in self.main_out_buffer.iter_mut().zip(main_buf) {
            dst.copy_from_slice(src.as_mut())
        }

        // Advance the playhead
        debug_assert!(self.render_config.is_some());
        self.main_playhead +=
            IFrames::new(*self.render_config.as_ref().unwrap().samples_per_channel as _);
    }
}
