use std::{num::NonZeroUsize, sync::mpsc};

use moire_ctrl::{circular_fifo, param};

use basedrop::Owned;

use crate::{
    graph::{ChannelBufferLayout, Node as _, RenderConfig},
    io, Clip, Deck, FrameSize, Frames, IFrames, Sample, SampleRate, Seconds, SAMPLE_ZERO,
};

const CHANNEL_BUFFER_LAYOUT: ChannelBufferLayout = ChannelBufferLayout::Disjunct;

#[cfg(target_arch = "x86")]
#[allow(deprecated)] // TODO: Use inline assembly
use std::arch::x86::{_mm_getcsr, _mm_setcsr, _MM_FLUSH_ZERO_ON};
#[cfg(target_arch = "x86_64")]
#[allow(deprecated)] // TODO: Use inline assembly
use std::arch::x86_64::{_mm_getcsr, _mm_setcsr, _MM_FLUSH_ZERO_ON};

#[allow(clippy::large_enum_variant)]
#[derive(Default)]
pub enum Command {
    /// Placeholder after consuming command_consumer in the realtime thread
    #[default]
    Empty,
    AddDeck {
        deck: Deck,
        storage: Owned<Vec<Deck>>,
    },
    LoadClip {
        deck_index: usize,
        clip: Clip,
        storage: Owned<Vec<Clip>>,
    },
    MoveClip {
        deck_index: usize,
        clip_index: usize,
        delta: Seconds,
    },
    SetInputParamValue {
        id: param::Id,
        value: param::Value,
    },
}

#[derive(Debug)]
pub enum Event {
    /// Events could not be emitted while the buffer was full
    /// and have been dropped.
    EventsDropped(NonZeroUsize),
    RenderConfigChanged(RenderConfig),
    PlayheadChanged(Seconds),
}

pub type CommandConsumer = circular_fifo::Consumer<Command>;

struct EventEmitter {
    producer: rtrb::Producer<Event>,
    drop_count: usize,
}

impl EventEmitter {
    const fn new(producer: rtrb::Producer<Event>) -> Self {
        Self {
            producer,
            drop_count: 0,
        }
    }

    fn emit_event(&mut self, event: Event) {
        if self.drop_count > 0 {
            let drop_count = unsafe { NonZeroUsize::new_unchecked(self.drop_count) };
            if self
                .producer
                .push(Event::EventsDropped(drop_count))
                .is_err()
            {
                self.drop_count += 1;
                return;
            }
            // Reset the counter after successfully sending an event
            self.drop_count = 0;
        }
        if self.producer.push(event).is_err() {
            self.drop_count += 1;
        }
    }
}

/// Engine is the central entry point into the audio processing code.
/// When the audio backend API (currently only JACK) requests new buffers
/// of audio to pass to the audio interface, Engine is responsible for
/// telling everything that generates audio to do so, then mixing the signals
/// into buffers that get passed to the audio backend (currently only
/// JackProcessHandler).
pub struct Engine {
    command_consumer: CommandConsumer,

    event_emitter: EventEmitter,

    io_event_rx: Option<mpsc::Receiver<io::Event>>,

    render_config: RenderConfig,

    main_playhead: IFrames,
    main_out_buffer: Vec<Vec<Sample>>,

    prelisten_playhead_offset: IFrames,
    prelisten_out_buffer: Vec<Vec<Sample>>,

    pub decks: Option<Owned<Vec<Deck>>>,
    deck_out_buffer: Vec<Vec<Sample>>,
}

impl Engine {
    pub const CHANNELS_PER_STREAM: usize = 2; // either stereo or dual mono

    pub fn new(command_consumer: CommandConsumer, event_tx: rtrb::Producer<Event>) -> Engine {
        let event_emitter = EventEmitter::new(event_tx);
        let deck_channels_out = Deck::channels_out(Self::CHANNELS_PER_STREAM);
        let deck_out_buffer = vec![Vec::<f32>::new(); deck_channels_out];
        let main_playhead = IFrames::new(0);
        let main_out_buffer = vec![Vec::<f32>::new(); Self::CHANNELS_PER_STREAM];
        let prelisten_playhead_offset = IFrames::new(0);
        let prelisten_out_buffer = vec![Vec::<f32>::new(); Self::CHANNELS_PER_STREAM];
        let render_config = RenderConfig {
            sample_rate: Default::default(),
            samples_per_channel: Default::default(),
            channel_buffer_layout: CHANNEL_BUFFER_LAYOUT,
        };
        Engine {
            main_out_buffer,
            prelisten_out_buffer,
            render_config,
            io_event_rx: None,
            event_emitter,
            command_consumer,
            main_playhead,
            prelisten_playhead_offset,
            decks: None,
            deck_out_buffer,
        }
    }

    pub fn render_config(&self) -> &RenderConfig {
        &self.render_config
    }

    fn emit_event(&mut self, event: Event) {
        self.event_emitter.emit_event(event);
    }

    fn handle_io_event(&mut self, event: io::Event) {
        match event {
            io::Event::SampleRateChanged(sample_rate) => {
                self.update_sample_rate(sample_rate.into())
            }
        }
    }

    /// Update the sample rate while already rendering.
    ///
    /// This method is real-time safe.
    fn update_sample_rate(&mut self, sample_rate: SampleRate) {
        if let Some(decks) = &mut self.decks {
            for deck in decks.iter_mut() {
                deck.update_sample_rate(sample_rate);
            }
        }
        self.render_config.sample_rate = sample_rate;
        self.emit_event(Event::RenderConfigChanged(self.render_config.clone()));
    }

    fn process_events(&mut self) {
        while let Some(event) = self.io_event_rx.as_mut().and_then(|rx| rx.try_recv().ok()) {
            self.handle_io_event(event);
        }
    }

    fn handle_command(&mut self, command: &mut Command) {
        let command = std::mem::take(command);
        match command {
            Command::Empty => (), // Should never happen, otherwise ignore silently
            Command::AddDeck {
                mut deck,
                mut storage,
            } => {
                if let Some(decks) = &mut self.decks {
                    storage.extend(decks.drain(..));
                }
                // The newly added deck might have missed the latest updates
                assert_no_alloc::permit_alloc(|| {
                    // This method might allocate
                    deck.prepare_to_render(&self.render_config, self.main_playhead)
                        .unwrap()
                });
                deck.set_prelisten_playhead_offset(self.prelisten_playhead_offset);
                storage.push(deck);
                self.decks = Some(storage);
            }
            Command::LoadClip {
                deck_index,
                clip,
                storage,
            } => {
                if let Some(deck) = self
                    .decks
                    .as_mut()
                    .and_then(|decks| decks.get_mut(deck_index))
                {
                    deck.load_clip(clip, storage);
                }
            }
            Command::MoveClip {
                deck_index,
                clip_index,
                delta,
            } => {
                // TODO: Use a consistent terminology.
                // The interpretation of `delta` is different for "moving"
                // and "seeking". Currently it matches the "seeking" behavior
                // (seeking forward (> 0) moves the clip to the left (backward))
                // but is wrong when considering it as "moving" the clip from the
                // perspective of an outside viewer.
                if let Some(deck) = self
                    .decks
                    .as_mut()
                    .and_then(|decks| decks.get_mut(deck_index))
                {
                    deck.seek_clip(clip_index, delta);
                }
            }
            Command::SetInputParamValue { id, value } => {
                // TODO: Generalize and optimize
                if let Some(decks) = &mut self.decks {
                    for deck in decks.iter_mut() {
                        // Translate global id to Deck-local index
                        let mut index = None;
                        for (param_index, param) in
                            deck.node_descriptor().params_in.iter().enumerate()
                        {
                            if param.id == id {
                                index = Some(param_index);
                                break;
                            }
                        }
                        if let Some(index) = index {
                            deck.set_input_param_value(index, value);
                        }
                    }
                }
            }
        }
    }

    fn process_commands(&mut self) {
        while let Some(mut item) = self.command_consumer.pop() {
            self.handle_command(&mut item);
            self.command_consumer.push_back(item);
        }
    }

    fn process_audio(&mut self) {
        debug_assert!(self.render_config.is_valid());
        debug_assert!(*self.render_config.samples_per_channel == self.main_out_buffer[0].len());
        debug_assert!(
            *self.render_config.samples_per_channel == self.prelisten_out_buffer[0].len()
        );

        for channel in self.main_out_buffer.iter_mut() {
            channel.fill(SAMPLE_ZERO);
        }
        for channel in self.prelisten_out_buffer.iter_mut() {
            channel.fill(SAMPLE_ZERO);
        }

        if let Some(decks) = &mut self.decks {
            for deck in decks.iter_mut() {
                deck.render(&mut self.deck_out_buffer);
                for (deck_channel, output_channel) in self.deck_out_buffer
                    [..self.main_out_buffer.len()]
                    .iter()
                    .zip(self.main_out_buffer.iter_mut())
                {
                    for (i, sample) in deck_channel.iter().enumerate() {
                        output_channel[i] += sample;
                    }
                }
                for (deck_channel, output_channel) in self.deck_out_buffer
                    [self.main_out_buffer.len()..]
                    .iter()
                    .zip(self.prelisten_out_buffer.iter_mut())
                {
                    for (i, sample) in deck_channel.iter().enumerate() {
                        output_channel[i] += sample;
                    }
                }
            }
        }

        self.main_playhead += IFrames::new(self.main_out_buffer[0].len() as _);
        let main_playhead_secs = Frames::from(self.main_playhead) / self.render_config.sample_rate;
        self.emit_event(Event::PlayheadChanged(main_playhead_secs));
    }

    pub fn process(&mut self) {
        #[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
        #[allow(deprecated)] // TODO: Use inline assembly
        unsafe {
            // See also: <https://github.com/rust-lang/stdarch/issues/852>
            const _MM_DENORMALS_ZERO_ON: u32 = 0x0040;
            _mm_setcsr(_mm_getcsr() | _MM_FLUSH_ZERO_ON | _MM_DENORMALS_ZERO_ON);
        }

        assert_no_alloc::assert_no_alloc(|| {
            // Things that have happened
            self.process_events();

            // Things that should happen
            self.process_commands();

            // The main duty
            self.process_audio();
        })
    }

    /// Attach the audio I/O backend.
    ///
    /// Supposed to be invoked only once during initialization.
    ///
    /// This method is not real-time safe.
    pub fn attach_io_backend(&mut self, rx: mpsc::Receiver<io::Event>, buffer_size: FrameSize) {
        debug_assert!(self.io_event_rx.is_none());
        debug_assert!(!self.render_config.is_valid());
        log::info!("Attaching audio I/O backend with initial buffer size {buffer_size}");
        self.io_event_rx = Some(rx);
        self.reset_buffer_size(buffer_size);
    }

    /// Reset the buffer size.
    ///
    /// The buffer size might change again after attaching the
    /// audio I/O backend.
    ///
    /// This method is not real-time safe.
    pub fn reset_buffer_size(&mut self, new_buffer_size: FrameSize) {
        debug_assert!(self.io_event_rx.is_some());
        let old_buffer_size = self.render_config.samples_per_channel;
        if old_buffer_size == new_buffer_size {
            // Unchanged
            return;
        }
        log::info!(
            "Changing buffer size from {old_buffer_size} to {new_buffer_size}",
            old_buffer_size = self.render_config.samples_per_channel,
        );
        let render_config = RenderConfig {
            samples_per_channel: new_buffer_size,
            ..self.render_config
        };

        for channel in &mut self.main_out_buffer {
            channel.resize(*render_config.samples_per_channel, SAMPLE_ZERO);
        }
        for channel in &mut self.prelisten_out_buffer {
            channel.resize(*render_config.samples_per_channel, SAMPLE_ZERO);
        }
        for channel in &mut self.deck_out_buffer {
            channel.resize(*render_config.samples_per_channel, SAMPLE_ZERO);
        }
        if let Some(decks) = &mut self.decks {
            for deck in decks.iter_mut() {
                deck.prepare_to_render(&render_config, self.main_playhead)
                    .unwrap();
            }
        }

        self.render_config = render_config;
        self.emit_event(Event::RenderConfigChanged(self.render_config.clone()));
    }

    pub fn main_out_buffer(&self) -> &[Vec<Sample>] {
        &self.main_out_buffer
    }

    pub fn prelisten_out_buffer(&self) -> &[Vec<Sample>] {
        &self.prelisten_out_buffer
    }
}
