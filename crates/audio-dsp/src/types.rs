//! Various newtypes for safe conversions between durations and frames.

use std::time::Duration;

/// Transport position/offset/distance in the frame/sample domain.
///
/// TODO (if needed): Split into different types for
///  - position (absolute)
///  - offset (relative, signed)
///  - count/distance (relative, unsigned)
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    PartialOrd,
    Default,
    derive_more::Display,
    derive_more::Deref,
    derive_more::Add,
    derive_more::AddAssign,
    derive_more::Sub,
    derive_more::SubAssign,
    derive_more::Neg,
)]
#[display(fmt = "{_0} \u{1F39E}")] // U+1F39E: Film Frames
#[repr(transparent)]
pub struct Frames(f64);

impl Frames {
    pub const ZERO: Self = Self(0.0);

    pub const fn new(inner: f64) -> Self {
        Self(inner)
    }

    pub const fn into_inner(self) -> f64 {
        let Self(inner) = self;
        inner
    }

    /// Unchecked cast to [`IFrames`].
    pub const fn to_int_unchecked(self) -> IFrames {
        let Self(inner) = self;
        IFrames(inner as _)
    }

    pub fn round_to_int(self) -> IFrames {
        IFrames(self.0.round().clamp(i64::MIN as _, i64::MAX as _) as i64)
    }

    pub fn floor_to_int(self) -> IFrames {
        IFrames(self.0.floor().clamp(i64::MIN as _, i64::MAX as _) as i64)
    }

    pub fn ceil_to_int(self) -> IFrames {
        IFrames(self.0.ceil().clamp(i64::MIN as _, i64::MAX as _) as i64)
    }
}

/// Integer transport position/offset/distance in the frame/sample domain.
///
/// TODO (if needed): Split into different types for
///  - position (absolute)
///  - offset (relative, signed)
///  - count/distance (relative, unsigned)
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Default,
    derive_more::Display,
    derive_more::Deref,
    derive_more::Add,
    derive_more::AddAssign,
    derive_more::Sub,
    derive_more::SubAssign,
    derive_more::Neg,
)]
#[display(fmt = "{_0} \u{1F39E}")] // U+1F39E: Film Frames
#[repr(transparent)]
pub struct IFrames(i64);

impl IFrames {
    pub const ZERO: Self = Self(0);

    pub const fn new(inner: i64) -> Self {
        Self(inner)
    }

    pub const fn into_inner(self) -> i64 {
        let Self(inner) = self;
        inner
    }

    pub const fn as_usize(self) -> usize {
        let inner = self.into_inner();
        debug_assert!(inner >= 0);
        let inner_as_usize = inner as usize;
        debug_assert!(inner == inner_as_usize as i64);
        inner_as_usize
    }
}

impl From<IFrames> for Frames {
    fn from(from: IFrames) -> Self {
        Self(*from as _)
    }
}

/// Specialization of `usize` for frames.
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Default,
    derive_more::Display,
    derive_more::Deref,
    derive_more::Add,
    derive_more::AddAssign,
    derive_more::Sub,
    derive_more::SubAssign,
)]
#[display(fmt = "{_0} \u{1F39E}")] // U+1F39E: Film Frames
#[repr(transparent)]
pub struct FrameSize(usize);

impl FrameSize {
    pub const ZERO: Self = Self(0);

    pub const fn new(inner: usize) -> Self {
        Self(inner)
    }

    pub const fn into_inner(self) -> usize {
        let Self(inner) = self;
        inner
    }
}

/// Transport position/offset/distance in the time domain.
///
/// TODO (if needed): Split into different types for
///  - position (absolute)
///  - offset (relative, signed)
///  - duration/distance (relative, unsigned)
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    PartialOrd,
    Default,
    derive_more::Display,
    derive_more::Deref,
    derive_more::Add,
    derive_more::AddAssign,
    derive_more::Sub,
    derive_more::SubAssign,
    derive_more::Neg,
)]
#[display(fmt = "{_0} s")]
#[repr(transparent)]
pub struct Seconds(f64);

impl Seconds {
    pub const ZERO: Self = Self(0.0);

    pub const fn new(inner: f64) -> Self {
        Self(inner)
    }

    pub const fn into_inner(self) -> f64 {
        let Self(inner) = self;
        inner
    }

    fn from_duration(duration: Duration) -> Self {
        Self(duration.as_secs_f64())
    }
}

impl From<Duration> for Seconds {
    fn from(from: Duration) -> Self {
        Self::from_duration(from)
    }
}

/// Sample rate in Hz.
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Default, derive_more::Display)]
#[display(fmt = "{hz} Hz")]
#[repr(transparent)]
pub struct SampleRate {
    hz: f64,
}

impl SampleRate {
    pub const ZERO: Self = Self { hz: 0.0 };

    pub fn from_hz(hz: f64) -> Self {
        // TODO: Declare as `const` after issue #57241
        // <https://github.com/rust-lang/rust/issues/57241>
        // has been resolved.
        debug_assert!(hz >= Self::ZERO.hz());
        Self { hz }
    }

    pub const fn hz(self) -> f64 {
        let Self { hz } = self;
        hz
    }
}

/// Integer sample rate in Hz.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default, derive_more::Display)]
#[display(fmt = "{hz} Hz")]
#[repr(transparent)]
pub struct ISampleRate {
    hz: u32,
}

impl ISampleRate {
    pub const ZERO: Self = Self { hz: 0 };

    pub const fn from_hz(hz: u32) -> Self {
        Self { hz }
    }

    pub const fn hz(self) -> u32 {
        let Self { hz } = self;
        hz
    }
}

impl From<ISampleRate> for SampleRate {
    fn from(from: ISampleRate) -> Self {
        Self {
            hz: f64::from(from.hz()),
        }
    }
}

pub type Sample = f32;

pub const SAMPLE_ZERO: Sample = 0f32;
