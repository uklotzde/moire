//! Atomic values that are missing in `std::sync::atomic` with limited functionality.

use std::sync::atomic::{AtomicU32, Ordering};

use crossbeam_utils::atomic::AtomicConsume;

#[derive(Debug)]
#[repr(transparent)]
pub struct AtomicF32 {
    bits: AtomicU32,
}

impl AtomicF32 {
    #[must_use]
    pub fn new(value: f32) -> Self {
        let bits = value.to_bits();
        Self {
            bits: AtomicU32::new(bits),
        }
    }

    #[must_use]
    pub fn load(&self, ordering: Ordering) -> f32 {
        let bits = self.bits.load(ordering);
        f32::from_bits(bits)
    }

    #[must_use]
    fn load_consume(&self) -> f32 {
        let bits = self.bits.load_consume();
        f32::from_bits(bits)
    }

    pub fn store(&self, value: f32, ordering: Ordering) {
        let bits = value.to_bits();
        self.bits.store(bits, ordering);
    }
}

impl AtomicConsume for AtomicF32 {
    type Val = f32;

    #[must_use]
    fn load_consume(&self) -> Self::Val {
        Self::load_consume(self)
    }
}
