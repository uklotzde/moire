use std::{ffi::OsStr, path::Path};

use audio_viz::{FilteredWaveformBin, WaveformFilter, WaveformFilterConfig};
use mime::Mime;
use symphonia::core::{
    audio::{AudioBufferRef, SampleBuffer},
    codecs::DecoderOptions,
    formats::FormatOptions,
    io::MediaSourceStream,
    meta::MetadataOptions,
    probe::Hint,
};

use super::FilteredWaveform;

const MIN_SAMPLE_RATE_HZ: u32 = 8000;

const MAX_SAMPLE_RATE_HZ: u32 = 192_000;

pub type WaveformFiltered = [FilteredWaveformBin];

pub struct WaveformAnalyzer<'a> {
    file_path: &'a Path,
    file_type: Option<&'a Mime>,
    bins_per_sec: f32,
    filter: WaveformFilter,
    waveform: FilteredWaveform,
}

impl<'a> WaveformAnalyzer<'a> {
    pub fn new(
        file_path: &'a Path,
        file_type: impl Into<Option<&'a Mime>>,
        bins_per_sec: f32,
    ) -> WaveformAnalyzer<'a> {
        WaveformAnalyzer {
            file_path,
            file_type: file_type.into(),
            bins_per_sec,
            filter: Default::default(),
            waveform: Default::default(),
        }
    }

    pub fn analyze(&mut self) -> anyhow::Result<()> {
        log::debug!(
            "Starting generation of waveform for {}",
            self.file_path.display()
        );

        let invoked_at = std::time::Instant::now();

        let file = Box::new(std::fs::File::open(self.file_path)?);
        let stream = MediaSourceStream::new(file, Default::default());
        let format_opts: FormatOptions = Default::default();
        let metadata_opts: MetadataOptions = Default::default();
        let decoder_opts: DecoderOptions = Default::default();

        let mut probe_hint = Hint::new();
        if let Some(file_ext) = self.file_path.extension().and_then(OsStr::to_str) {
            probe_hint.with_extension(file_ext);
        }
        if let Some(file_type) = &self.file_type {
            probe_hint.mime_type(file_type.essence_str());
        }
        let probe_result = symphonia::default::get_probe().format(
            &probe_hint,
            stream,
            &format_opts,
            &metadata_opts,
        )?;

        let mut reader = probe_result.format;
        let codec_params = &reader
            .default_track()
            .ok_or_else(|| anyhow::anyhow!("No default audio stream found"))?
            .codec_params;
        let mut decoder = symphonia::default::get_codecs().make(codec_params, &decoder_opts)?;

        let Some(sample_rate_hz) = codec_params.sample_rate else {
            anyhow::bail!("Unknown sample rate");
        };
        if !(MIN_SAMPLE_RATE_HZ..=MAX_SAMPLE_RATE_HZ).contains(&sample_rate_hz) {
            anyhow::bail!("Invalid sample rate {sample_rate_hz} Hz");
        }

        let filter_config = WaveformFilterConfig {
            sample_rate_hz: sample_rate_hz as f32,
            bins_per_sec: self.bins_per_sec,
            ..Default::default()
        };
        self.filter = WaveformFilter::new(filter_config);

        let total_frames = codec_params.n_frames.unwrap_or(0);
        let total_secs = total_frames as f64 / sample_rate_hz as f64;
        let total_bins = f64::from(self.bins_per_sec) * total_secs;
        let max_bin_count = (total_bins as usize).saturating_add(1);
        self.waveform = Vec::with_capacity(max_bin_count);

        loop {
            let packet = match reader.next_packet() {
                Ok(packet) => packet,
                Err(err) => {
                    if let symphonia::core::errors::Error::IoError(err) = &err {
                        if matches!(err.kind(), std::io::ErrorKind::UnexpectedEof) {
                            // EOF
                            log::debug!("End of file");
                            break;
                        }
                    }
                    return Err(err.into());
                }
            };
            let audio_buffer = decoder.decode(&packet)?;
            self.process(audio_buffer)?;
        }
        if let Some(last_bin) = std::mem::take(&mut self.filter).finish() {
            self.waveform.push(last_bin);
        }

        log::debug!(
            "Generated waveform for {file_path} in {elapsed_ms} ms",
            file_path = self.file_path.display(),
            elapsed_ms = invoked_at.elapsed().as_millis(),
        );

        Ok(())
    }

    fn process(&mut self, buffer_ref: AudioBufferRef) -> anyhow::Result<()> {
        let frames = buffer_ref.frames();
        let channels = buffer_ref.spec().channels.count();

        let mut buffer =
            SampleBuffer::<f32>::new(buffer_ref.capacity().try_into()?, *buffer_ref.spec());
        buffer.copy_interleaved_ref(buffer_ref);

        for f in 0..frames {
            let mut mono_sum = 0f32;
            for c in 0..channels {
                mono_sum += buffer.samples()[f + c];
            }
            let mono_sample = mono_sum / channels as f32;
            if let Some(next_bin) = self.filter.add_sample(mono_sample) {
                self.waveform.push(next_bin);
            }
        }

        Ok(())
    }

    pub fn waveform(&self) -> &WaveformFiltered {
        &self.waveform
    }
}
