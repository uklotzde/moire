//! Synchronous callbacks
//!
//! Could be attached to UI controls.

use std::{
    future::Future,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use aoide::{
    backend_embedded::media::predefined_faceted_tag_mapping_config,
    desktop_app::{collection, fs::choose_directory, Handle},
    media_file::io::import::ImportTrackConfig,
};
use unnest::some_or_return;

use crate::{
    search::parse_track_search_filter_from_input, Library, TRACK_REPO_SEARCH_PREFETCH_LIMIT,
};

// Simplified, local state management. A more sophisticated solution
// that enables/disables the corresponding buttons accordingly would
// require an application-wide global state management.
static ON_CHOOSE_MUSIC_DIR_GUARD: AtomicBool = AtomicBool::new(false);

#[allow(clippy::missing_panics_doc)] // Infallible
pub fn on_music_dir_choose(library: &Library, tokio_rt: tokio::runtime::Handle) -> impl FnMut() {
    let settings_state = Arc::clone(library.state().settings());
    move || {
        if ON_CHOOSE_MUSIC_DIR_GUARD
            .compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed)
            .unwrap_or(true)
        {
            // Prevent opening multiple file dialogs at the same time
            log::debug!("Already choosing a music directory");
            return;
        }
        log::info!("Choosing music directory...");
        let settings_state = Arc::clone(&settings_state);
        tokio_rt.spawn(async move {
            let old_music_dir = settings_state.read().music_dir.clone();
            let new_music_dir = choose_directory(old_music_dir.as_deref()).await;
            if let Some(music_dir) = &new_music_dir {
                log::info!("Updating music directory: {}", music_dir.display());
                settings_state.try_update_music_dir(Some(music_dir));
            }
            ON_CHOOSE_MUSIC_DIR_GUARD
                .compare_exchange(true, false, Ordering::Release, Ordering::Relaxed)
                .expect("infallible");
        });
    }
}

pub fn on_music_dir_reset(library: &Library) -> impl FnMut() {
    let settings_state = Arc::clone(library.state().settings());
    move || {
        log::info!("Resetting music directory");
        settings_state.try_update_music_dir(None);
    }
}

pub fn on_collection_synchronize_music_dir(
    library: &Library,
    tokio_rt: tokio::runtime::Handle,
) -> impl FnMut() {
    let handle = Handle::downgrade(library.handle());
    let collection_state = Arc::downgrade(library.state().collection());
    move || {
        let report_progress_fn = |progress: Option<
            aoide::backend_embedded::batch::synchronize_collection_vfs::Progress,
        >| {
            // TODO: The reporting probably needs to be debounced, otherwise it
            // could overload the UI. The synchronous invocation will also slow
            // down the batch task if it takes too much time.
            if let Some(progress) = progress {
                log::warn!("TODO Report progress while synchronizing collection: {progress:?}");
            } else {
                log::warn!("TODO Report synchronizing collection finished");
            }
        };
        let collection_state = some_or_return!(collection_state.upgrade());
        let handle = some_or_return!(handle.upgrade());
        let Some(task) =
            collection_synchronize_music_dir_task(handle, collection_state, report_progress_fn)
        else {
            return;
        };
        tokio_rt.spawn(task);
    }
}

fn collection_synchronize_music_dir_task(
    handle: Handle,
    state: Arc<collection::ObservableState>,
    mut report_progress_fn: impl FnMut(Option<aoide::backend_embedded::batch::synchronize_collection_vfs::Progress>)
        + Clone
        + Send
        + 'static,
) -> Option<impl Future<Output = ()> + Send + 'static> {
    log::debug!("Synchronizing collection with music directory...");
    let import_track_config = ImportTrackConfig {
        // TODO: Customize faceted tag mapping
        faceted_tag_mapping: predefined_faceted_tag_mapping_config(),
        ..Default::default()
    };
    let task_report_progress_fn = {
        let mut report_progress_fn = report_progress_fn.clone();
        move |progress| {
            report_progress_fn(Some(progress));
        }
    };
    // See the aoide-demo-app on how to properly handle aborting this long running task!
    let abort_flag = Arc::new(AtomicBool::new(false));
    let (task, continuation) = state.try_synchronize_vfs_task(
        &handle,
        import_track_config,
        task_report_progress_fn,
        abort_flag,
    )?;
    let non_abortable_task = async move {
        let result = task.await;
        report_progress_fn(None);
        log::debug!("Synchronizing collection with music directory completed: {result:?}");
        let outcome = state.synchronize_vfs_task_joined(result.into(), continuation);
        if let Some(outcome) = outcome {
            log::debug!("Synchronized collection with music directory: {outcome:?}");
        }
        let Some((task, continuation)) = state.try_refresh_from_db_task(&handle) else {
            log::debug!(
                "Cannot refresh collection from database after resynchronizing music directory"
            );
            return;
        };
        log::debug!("Refreshing collection from database after resynchronizing music directory");
        let result = task.await;
        state.refresh_from_db_task_completed(result, continuation);
    };
    Some(non_abortable_task)
}

pub fn on_track_search_submit_input(library: &Library) -> impl FnMut(String) -> String {
    let track_search_state = Arc::clone(library.state.track_search());
    move |input| {
        log::debug!("Received search input: {input}");
        let input = input.trim().to_owned();
        let filter = parse_track_search_filter_from_input(&input);
        let resolve_url_from_content_path = track_search_state
            .read()
            .default_params()
            .resolve_url_from_content_path
            .clone();
        let mut params = aoide::api::track::search::Params {
            filter,
            ordering: vec![], // TODO
            resolve_url_from_content_path,
        };
        // Argument is consumed when updating succeeds
        if track_search_state.try_update_params(&mut params) {
            log::debug!("Track search params updated: {params:?}");
        } else {
            log::debug!("Track search params not updated: {params:?}");
        }
        input
    }
}

pub fn on_track_search_fetch_more(
    library: &Library,
    tokio_rt: tokio::runtime::Handle,
) -> impl FnMut() {
    let handle = library.handle().downgrade();
    let state = Arc::downgrade(library.state.track_search());
    move || {
        let handle = some_or_return!(handle.upgrade());
        let state = some_or_return!(state.upgrade());
        let Some((task, continuation)) =
            state.try_fetch_more_task(&handle, Some(TRACK_REPO_SEARCH_PREFETCH_LIMIT))
        else {
            log::debug!("Cannot fetch more search results");
            return;
        };
        log::debug!("Fetching more search results...");
        tokio_rt.spawn(async move {
            let result = task.await;
            if state.fetch_more_task_joined(result.into(), continuation) {
                log::debug!("Fetched more search results");
            }
        });
    }
}
