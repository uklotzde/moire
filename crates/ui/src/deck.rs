use std::{
    cell::RefCell,
    rc::Rc,
    str::FromStr,
    sync::{Arc, Mutex},
};

use aoide::{desktop_app::Handle, TrackUid};
use audio_viz::FilteredWaveformBin;
use basedrop::Owned;
use unnest::{ok_or_return, some_or_return};

use slint::{ComponentHandle, Model, ModelRc, SharedString, VecModel};

use moire::waveform::analyzer::WaveformAnalyzer;
use moire_library::{load_tracks_from_current_collection_by_uid, Library};

use moire_audio_dsp::{
    deck, engine as audio_engine,
    graph::{Node as AudioNode, RenderConfig},
    Clip as AudioClip, Deck as AudioDeck, Seconds,
};

use moire_audio_io::jack::{self, JackBackend};

use moire_ctrl::{
    circular_fifo,
    param::{
        self,
        register::{Registration, RegistrationHeader},
        Registry as ParamRegistry,
    },
};

use moire_ui_generated::{ClipProps, DeckProps, MainWindow};

fn on_input_value_changed<T>(
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    param_registry: &Arc<Mutex<ParamRegistry>>,
    deck_index_address_fn: impl Fn(i32) -> param::Address<'static>,
) -> impl FnMut(i32, T)
where
    T: Into<param::Value>,
{
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    let param_registry = Arc::downgrade(param_registry);
    let mut deck_index_to_param_id: Vec<Option<param::Id>> = Vec::new();
    move |deck_index, value| {
        let to_audio_tx = some_or_return!(to_audio_tx.upgrade());
        debug_assert!(deck_index >= 0);
        let deck_index = deck_index as usize;
        let min_len = deck_index + 1;
        deck_index_to_param_id.resize(min_len.max(deck_index_to_param_id.len()), None);
        debug_assert!(deck_index < deck_index_to_param_id.len());
        #[allow(unsafe_code)]
        let param_id_entry = unsafe { deck_index_to_param_id.get_unchecked_mut(deck_index) };
        let param_id = if let Some(param_id) = param_id_entry {
            *param_id
        } else {
            let param_registry = some_or_return!(param_registry.upgrade());
            let mut param_registry = ok_or_return!(param_registry.lock());
            let address = deck_index_address_fn(deck_index as i32);
            let Registration {
                header: RegistrationHeader { id: param_id, .. },
                ..
            } = param_registry.register_address(address);
            *param_id_entry = Some(param_id);
            param_id
        };
        let param_value = value.into();
        log::trace!("Deck #{deck_index}: SetInputParamValue {param_id:?} -> {param_value:?}");
        let command = audio_engine::Command::SetInputParamValue {
            id: param_id,
            value: param_value,
        };
        to_audio_tx.lock().unwrap().push(command);
    }
}

pub(crate) fn on_volume_changed(
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    param_registry: &Arc<Mutex<ParamRegistry>>,
) -> impl FnMut(i32, f32) {
    on_input_value_changed(to_audio_tx, param_registry, |deck_index| {
        deck::gain_address(&AudioDeck::param_address_prefix(
            param::Direction::Input,
            deck_index as u8,
        ))
    })
}

pub(crate) fn on_tempo_changed(
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    param_registry: &Arc<Mutex<ParamRegistry>>,
) -> impl FnMut(i32, f32) {
    on_input_value_changed(to_audio_tx, param_registry, |deck_index| {
        deck::tempo_address(&AudioDeck::param_address_prefix(
            param::Direction::Input,
            deck_index as u8,
        ))
    })
}

pub(crate) fn on_headphones_changed(
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    param_registry: &Arc<Mutex<ParamRegistry>>,
) -> impl FnMut(i32, bool) {
    on_input_value_changed(to_audio_tx, param_registry, |deck_index| {
        deck::prelisten_address(&AudioDeck::param_address_prefix(
            param::Direction::Input,
            deck_index as u8,
        ))
    })
}

pub(crate) fn on_add_deck(
    main_window: &MainWindow,
    render_config: &Rc<RefCell<RenderConfig>>,
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    mut to_jack_tx: rtrb::Producer<jack::Command>,
    jack_backend: &Rc<JackBackend>,
    gc_handle: &basedrop::Handle,
    param_registry: &Arc<Mutex<ParamRegistry>>,
) -> impl FnMut() {
    let main_window = main_window.as_weak();
    let render_config = Rc::downgrade(render_config);
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    let gc_handle = gc_handle.clone();
    let jack_backend = Rc::downgrade(jack_backend);
    let param_registry = Arc::downgrade(param_registry);
    move || {
        let main_window = main_window.upgrade().unwrap();
        let param_registry = some_or_return!(param_registry.upgrade());

        let decks = main_window.get_decks();
        let deck_count = match decks.as_any().downcast_ref::<VecModel<DeckProps>>() {
            None => {
                let model = VecModel::from(vec![empty_deckdata()]);
                main_window.set_decks(ModelRc::new(model));
                1
            }
            Some(decks) => {
                decks.push(empty_deckdata());
                decks.row_count()
            }
        };

        // The deck index is 0-based
        debug_assert!(deck_count > 0);
        let deck_index = deck_count - 1;
        log::debug!("Preparing to add deck #{deck_index}");

        let jack_backend = jack_backend.upgrade().unwrap();
        let jack_client = jack_backend.client();
        let port_l = jack_client
            .register_port(&format!("deck{deck_index}_L"), Default::default())
            .unwrap();
        let port_r = jack_client
            .register_port(&format!("deck{deck_index}_R"), Default::default())
            .unwrap();
        let vec_l = Owned::new(&gc_handle, Vec::with_capacity(deck_count));
        let vec_r = Owned::new(&gc_handle, Vec::with_capacity(deck_count));
        to_jack_tx
            .push(jack::Command::AddDeck {
                port_l,
                port_r,
                vec_l,
                vec_r,
            })
            .unwrap();

        let mut new_deck = AudioDeck::new(deck_index as u8, 2);
        {
            let mut param_registry = ok_or_return!(param_registry.lock());
            new_deck
                .initialize(&mut param_registry)
                .expect("initialize");
        }
        let render_config = render_config.upgrade().unwrap();
        new_deck
            .prepare_to_render(&render_config.borrow(), Default::default())
            .unwrap();

        let storage = Vec::<AudioDeck>::with_capacity(deck_count);

        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        to_audio_tx.push(audio_engine::Command::AddDeck {
            deck: new_deck,
            storage: Owned::new(&gc_handle, storage),
        });
    }
}

// 200 ms per bin
const WAVEFORM_BINS_PER_SEC: f32 = 5.0;

pub(crate) fn on_load_file(
    main_window: &MainWindow,
    library: &Library,
    tokio_rt: tokio::runtime::Handle,
    render_config: &Rc<RefCell<RenderConfig>>,
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    gc_handle: &basedrop::Handle,
) -> impl FnMut(i32, f32, SharedString) {
    let main_window = main_window.as_weak();
    let lib_handle = Handle::downgrade(library.handle());
    let lib_collection_state = Arc::downgrade(library.state().collection());
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    let render_config = Rc::downgrade(render_config);
    let gc_handle = gc_handle.clone();
    move |index, time, track_uid| {
        let invoked_at = std::time::Instant::now();

        if track_uid.is_empty() {
            log::warn!("Deck {index}: No track selected for loading");
            return;
        }
        log::debug!("Deck {index}: Loading track '{track_uid}' from library");
        let track_uid = TrackUid::from_str(&track_uid).expect("valid track UID");

        let main_window = main_window.clone();
        let lib_handle = lib_handle.clone();
        let lib_collection_state = lib_collection_state.clone();
        let to_audio_tx = to_audio_tx.clone();
        let gc_handle = gc_handle.clone();
        // TODO: Use atomics wrapped into an Arc for buffer size and sample rate if they
        // should be evaluated just before actually loading the track, i.e. after finishing
        // the asynchronous load from the library and re-entering the UI thread. Tunneling
        // them through the executor thread requires Send although they are only ever
        // accessed in the UI thread and never in the executor thread.
        // Until the requirements are clear simply read out their values now and accept that
        // they might change at any time before actually loading the track into the deck.
        let render_config = render_config.upgrade().unwrap().borrow().clone();
        tokio_rt.spawn(async move {
            let lib_handle = some_or_return!(lib_handle.upgrade());
            let lib_collection_state = some_or_return!(lib_collection_state.upgrade());
            let track_entity = if let Some(track_entity) = load_tracks_from_current_collection_by_uid(&lib_handle, &lib_collection_state, vec![track_uid.clone()]).await
            .map_err(|err| {
                log::error!("Deck {index}: Failed to load track '{track_uid}' from library: {err}");
            })
            .ok()
            .and_then(|v| v.into_iter().next())
            {
                track_entity
            } else {
                log::warn!("Deck {index}: Track '{track_uid}' not found in library");
                return;
            };

            let file_path = track_entity
                .body
                .content_url
                .as_ref()
                .and_then(|content_url| content_url.to_file_path().ok())
                .expect("local file URL");
            let media_type = &track_entity.body.track.media_source.content.r#type;

            let elapsed_since_invoked = invoked_at.elapsed();
            log::debug!(
                "Deck {index}: Loading track '{file_path}' from library took {elapsed_since_invoked_millis} ms",
                file_path = file_path.display(),
                elapsed_since_invoked_millis = elapsed_since_invoked.as_millis(),
            );

            let mut waveform_analyzer = WaveformAnalyzer::new(&file_path, media_type, WAVEFORM_BINS_PER_SEC);
            tokio::task::block_in_place(|| {
                if let Err(err) = waveform_analyzer.analyze() {
                    log::warn!("Failed to analyze audio file \"{file_path}\": {err}", file_path = file_path.display());
                }
            });
            let waveform = waveform_analyzer.waveform();
            if waveform.is_empty() {
                log::warn!("No waveform data");
                return;
            }
            let waveform_buffer = render_waveform(waveform);

            main_window.upgrade_in_event_loop(move |main_window| {
                // FIXME: Is the deck still supposed to load the track that has now
                // become available, i.e. is a load operation for the corresponding
                // track uid still pending? Otherwise we should abort this operation
                // and discard the loaded track! Finite state machines are inevitable
                // in asynchronous systems for predictable behavior.

                let elapsed_since_invoked = invoked_at.elapsed();
                let start_on_timeline_seconds = time + elapsed_since_invoked.as_secs_f32();
                let start_on_timeline = Seconds::new(start_on_timeline_seconds as _);
                let mut clip = match AudioClip::new(
                    start_on_timeline,
                    &file_path,
                    Seconds::ZERO,
                    None,
                ) {
                    Ok(clip) => clip,
                    Err(err) => {
                        log::warn!("Failed to create clip: {err}");
                        return;
                    }
                };
                log::debug!("Preparing audio clip: {file_path}", file_path = file_path.display());
                // Avoid allocations in the real-time context by preparing to render here
                clip.prepare_to_render(&render_config);

                let decks = main_window.get_decks();
                let decks = decks
                    .as_any()
                    .downcast_ref::<VecModel<DeckProps>>()
                    .unwrap();
                let deck = decks.row_data(index as usize).unwrap_or_default();

                let clips = deck
                    .clips
                    .as_any()
                    .downcast_ref::<VecModel<ClipProps>>()
                    .unwrap();
                clips.push(ClipProps {
                    start_on_timeline_seconds,
                    waveform: slint::Image::from_rgba8_premultiplied(waveform_buffer),
                });

                let storage = Vec::with_capacity(clips.row_count());

                let to_audio_tx = to_audio_tx.upgrade().unwrap();
                let mut to_audio_tx = to_audio_tx.lock().unwrap();
                log::debug!("Loading audio clip: {clip_props:?}", clip_props = clip.props());
                to_audio_tx
                    .push(audio_engine::Command::LoadClip {
                        deck_index: index as usize,
                        clip,
                        storage: Owned::new(&gc_handle, storage),
                    });
            }).unwrap();
        });
    }
}

pub(crate) fn on_clip_seek(
    to_audio_tx: &Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, i32, f32) {
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    move |deck_index, clip_index, diff_seconds| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        let delta = Seconds::new(diff_seconds as _);
        to_audio_tx.push(audio_engine::Command::MoveClip {
            deck_index: deck_index as usize,
            clip_index: clip_index as usize,
            delta,
        });
    }
}

fn empty_deckdata() -> DeckProps {
    DeckProps {
        clips: ModelRc::new(VecModel::from(vec![])),
    }
}

const WAVEFORM_BACKGROUND_COLOR: tiny_skia::Color = tiny_skia::Color::TRANSPARENT;

fn render_waveform(
    waveform_data: &[FilteredWaveformBin],
) -> slint::SharedPixelBuffer<slint::Rgba8Pixel> {
    let height = 200;
    let width = waveform_data.len() as u32;
    log::debug!("Rendering waveform: {width}x{height}");
    let mut buffer = slint::SharedPixelBuffer::<slint::Rgba8Pixel>::new(width, height);
    let mut pixmap =
        tiny_skia::PixmapMut::from_bytes(buffer.make_mut_bytes(), width, height).unwrap();

    pixmap.fill(WAVEFORM_BACKGROUND_COLOR);

    let mut paint = tiny_skia::Paint {
        anti_alias: false,
        ..tiny_skia::Paint::default()
    };
    let stroke = Default::default();
    for (x, bin) in waveform_data.iter().enumerate() {
        let energy = bin.energy();
        let amplitude = energy.all.to_f32();
        let (color_r, color_g, color_b) = energy.spectral_rgb_color_all();
        let color = tiny_skia::Color::from_rgba(color_r, color_g, color_b, 1f32).unwrap();
        let path = {
            let mut path_builder = tiny_skia::PathBuilder::with_capacity(2, 2);
            path_builder.move_to(x as f32, height as f32);
            path_builder.line_to(x as f32, height as f32 * (1.0 - amplitude));
            path_builder.finish().unwrap()
        };
        paint.set_color(color);
        pixmap.stroke_path(
            &path,
            &paint,
            &stroke,
            tiny_skia::Transform::identity(),
            None,
        );
    }

    buffer
}
