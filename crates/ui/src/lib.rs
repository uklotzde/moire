use std::{
    cell::RefCell,
    path::PathBuf,
    rc::Rc,
    sync::{Arc, Mutex},
};

use aoide::desktop_app::{settings, Handle};
use slint::ComponentHandle as _;

use moire_audio_dsp::{engine as audio_engine, graph::RenderConfig, Seconds};
use moire_audio_io::jack::{self, JackBackend};
use moire_ctrl::{circular_fifo, param::Registry};
use moire_library::Library;
use moire_ui_generated as ui;

mod deck;
mod library;
mod util;

pub struct Gui {
    main_window: ui::MainWindow,
    _tokio_rt: tokio::runtime::Handle,
    _library: Library,
    _poll_audio_rx_timer: slint::Timer,
    _render_config: Rc<RefCell<RenderConfig>>,
    _to_audio_tx: Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    _jack_backend: Rc<JackBackend>,
    _shared_param_registry: Arc<Mutex<Registry>>,
}

/// Gui is responsible for running the GUI after the rest of the
/// application has been initialized.
impl Gui {
    #[allow(clippy::too_many_arguments)] // FIXME
    pub fn new(
        config_dir: PathBuf,
        tokio_rt: tokio::runtime::Handle,
        aoide_handle: Handle,
        initial_settings: settings::State,
        initial_render_config: RenderConfig,
        to_audio_tx: circular_fifo::Producer<audio_engine::Command>,
        from_audio_rx: rtrb::Consumer<audio_engine::Event>,
        to_jack_tx: rtrb::Producer<jack::Command>,
        jack_backend: Rc<JackBackend>,
        gc_handle: basedrop::Handle,
    ) -> Gui {
        let main_window = ui::MainWindow::new().expect("MainWindow");

        let library = Library::new(aoide_handle, initial_settings);
        library.spawn_background_tasks(&tokio_rt, config_dir);
        library::spawn_ui_tasks(&main_window, &tokio_rt, &library);
        library::connect_ui_callbacks(&main_window, &tokio_rt, &library);

        let to_audio_tx = Arc::new(Mutex::new(to_audio_tx));
        let shared_param_registry = Arc::new(Mutex::new(Registry::default()));

        main_window.on_volume_changed_on_deck(deck::on_volume_changed(
            &to_audio_tx,
            &shared_param_registry,
        ));
        main_window
            .on_tempo_changed_on_deck(deck::on_tempo_changed(&to_audio_tx, &shared_param_registry));

        let render_config = Rc::new(RefCell::new(initial_render_config));
        let mut add_deck_callback = deck::on_add_deck(
            &main_window,
            &render_config,
            &to_audio_tx,
            to_jack_tx,
            &jack_backend,
            &gc_handle,
            &shared_param_registry,
        );
        // Add the first deck implicitly...
        add_deck_callback();
        // ...before attaching the callback to the UI.
        main_window.on_add_deck(add_deck_callback);

        main_window.on_load_file_to_deck(deck::on_load_file(
            &main_window,
            &library,
            tokio_rt.clone(),
            &render_config,
            &to_audio_tx,
            &gc_handle,
        ));

        main_window.on_clip_seek_on_deck(deck::on_clip_seek(&to_audio_tx));

        main_window.on_headphones_changed_on_deck(deck::on_headphones_changed(
            &to_audio_tx,
            &shared_param_registry,
        ));

        let poll_audio_rx_timer = slint::Timer::default();
        poll_audio_rx_timer.start(
            slint::TimerMode::Repeated,
            std::time::Duration::from_millis(100),
            on_gui_poll_timer(&main_window, &render_config, from_audio_rx),
        );

        Gui {
            main_window,
            _tokio_rt: tokio_rt,
            _library: library,
            _poll_audio_rx_timer: poll_audio_rx_timer,
            _render_config: render_config,
            _to_audio_tx: to_audio_tx,
            _jack_backend: jack_backend,
            _shared_param_registry: shared_param_registry,
        }
    }

    pub fn run(&self) -> anyhow::Result<()> {
        self.main_window.run().map_err(Into::into)
    }
}

fn on_gui_poll_timer(
    main_window: &ui::MainWindow,
    render_config: &Rc<RefCell<RenderConfig>>,
    mut from_audio_rx: rtrb::Consumer<audio_engine::Event>,
) -> impl FnMut() {
    let main_window = main_window.as_weak();
    let render_config = Rc::downgrade(render_config);
    move || {
        let mut last_playhead_secs = None;
        while from_audio_rx.slots() > 0 {
            match from_audio_rx.pop().unwrap() {
                audio_engine::Event::EventsDropped(drop_count) => {
                    log::error!(
                        "Dropped {drop_count} events in realtime context that could not be emitted"
                    );
                }
                audio_engine::Event::PlayheadChanged(playhead_secs) => {
                    last_playhead_secs = Some(playhead_secs)
                }
                audio_engine::Event::RenderConfigChanged(new_render_config) => {
                    let render_config = render_config.upgrade().unwrap();
                    *render_config.borrow_mut() = new_render_config;
                }
            }
        }
        if let Some(playhead_secs) = last_playhead_secs {
            debug_assert!(playhead_secs >= Seconds::ZERO);
            let main_window = main_window.upgrade().unwrap();
            main_window.set_time(*playhead_secs as _);
            let hours = (*playhead_secs / 3600.0).floor();
            let minutes = (*playhead_secs / 60.0).floor();
            let seconds = (*playhead_secs % 60.0).floor();
            let deciseconds = ((*playhead_secs - playhead_secs.floor()) * 10.0).floor();
            let string = format!("{hours}:{minutes}:{seconds}.{deciseconds}");
            main_window.set_duration_text(string.into());
        }
    }
}
