use std::future::Future;

use aoide::desktop_app::collection;
use discro::tasklet::OnChanged;
use slint::Weak;

use crate::{ui, util::u64_to_i32_saturating};

#[must_use]
fn update_properties_in_event_loop(
    ui_handle: &Weak<ui::MainWindow>,
    state: &collection::State,
) -> OnChanged {
    log::debug!("Updating collection properties: {state:?}");
    let is_idle = !state.is_pending();
    let is_ready;
    let entity_uid;
    let tracks_count;
    let playlists_count;
    match state {
        collection::State::Ready { entity, summary } => {
            is_ready = true;
            entity_uid = entity.hdr.uid.to_string();
            tracks_count = u64_to_i32_saturating(summary.tracks.total_count);
            playlists_count = u64_to_i32_saturating(summary.playlists.total_count);
        }
        _ => {
            is_ready = false;
            entity_uid = state
                .entity_uid()
                .map(ToString::to_string)
                .unwrap_or_default();
            tracks_count = 0;
            playlists_count = 0;
        }
    };
    ui_handle
        .upgrade_in_event_loop({
            move |ui| {
                ui.set_lib_collection_is_idle(is_idle);
                ui.set_lib_collection_is_ready(is_ready);
                ui.set_lib_collection_uid(entity_uid.into());
                ui.set_lib_collection_tracks_count(tracks_count);
                ui.set_lib_collection_playlists_count(playlists_count);
            }
        })
        .map(|()| OnChanged::Continue)
        .unwrap_or(OnChanged::Abort)
}

pub fn on_state_changed_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    library: &crate::Library,
) -> impl Future<Output = ()> + Send + 'static {
    let mut subscriber = library.state().collection().subscribe_changed();
    async move {
        log::debug!("Starting on_state_changed_property_updater");
        loop {
            match update_properties_in_event_loop(&ui_handle, &subscriber.read_ack()) {
                OnChanged::Continue => (),
                OnChanged::Abort => break,
            }
            log::debug!("Suspending on_state_changed_property_updater");
            if subscriber.changed().await.is_err() {
                break;
            }
            log::debug!("Resuming on_state_changed_property_updater");
        }
        log::debug!("Stopping on_state_changed_property_updater");
    }
}
