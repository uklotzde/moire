use moire_library::{callback, Library};

use moire_ui_generated as ui;
use slint::ComponentHandle as _;

mod collection;
mod music_dir;
mod track_search;

/// Library -> UI: Update UI properties when library state changes
pub fn spawn_ui_tasks(
    main_window: &ui::MainWindow,
    tokio_rt: &tokio::runtime::Handle,
    library: &Library,
) {
    tokio_rt.spawn(music_dir::on_changed_property_updater(
        main_window.as_weak(),
        library,
    ));
    tokio_rt.spawn(collection::on_state_changed_property_updater(
        main_window.as_weak(),
        library,
    ));
    tokio_rt.spawn(track_search::on_state_changed_property_updater(
        main_window.as_weak(),
        library,
    ));
}

/// UI -> Library: Connect library callbacks to UI controls
pub fn connect_ui_callbacks(
    main_window: &ui::MainWindow,
    tokio_rt: &tokio::runtime::Handle,
    library: &Library,
) {
    main_window.on_lib_music_dir_choose(callback::on_music_dir_choose(library, tokio_rt.clone()));
    main_window.on_lib_music_dir_reset(callback::on_music_dir_reset(library));
    main_window.on_lib_collection_synchronize_music_dir(
        callback::on_collection_synchronize_music_dir(library, tokio_rt.clone()),
    );
    main_window.on_lib_track_search_submit_input({
        let mut on_track_search_submit_input = callback::on_track_search_submit_input(library);
        move |input| on_track_search_submit_input(input.into()).into()
    });
    main_window.on_lib_track_search_fetch_more(callback::on_track_search_fetch_more(
        library,
        tokio_rt.clone(),
    ));
}
