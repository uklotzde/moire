use std::future::Future;

use aoide::desktop_app::{fs::DirPath, settings};
use discro::tasklet::OnChanged;
use slint::Weak;

use crate::ui;

#[must_use]
fn update_properties_in_event_loop(
    ui_handle: &Weak<ui::MainWindow>,
    music_dir: Option<&DirPath<'_>>,
) -> OnChanged {
    log::debug!("Updating music directory properties: {music_dir:?}");
    let music_dir_path = music_dir
        .map(|dir_path| dir_path.display().to_string())
        .unwrap_or_default();
    ui_handle
        .upgrade_in_event_loop({
            move |ui| {
                ui.set_lib_music_dir_is_valid(!music_dir_path.is_empty());
                ui.set_lib_music_dir_path(music_dir_path.into());
            }
        })
        .map(|()| OnChanged::Continue)
        .unwrap_or(OnChanged::Abort)
}

pub fn on_changed_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    library: &crate::Library,
) -> impl Future<Output = ()> + Send + 'static {
    let subscriber = library.state().settings().subscribe_changed();
    settings::tasklet::on_music_dir_changed(subscriber, move |music_dir| {
        update_properties_in_event_loop(&ui_handle, music_dir)
    })
}
