use std::future::Future;

use aoide::desktop_app::track;
use aoide::media::artwork::{Artwork, ArtworkImage, EmbeddedArtwork};
use discro::tasklet::OnChanged;
use moire_library::Library;
use slint::{
    Color, Image, Model, ModelRc, Rgb8Pixel, SharedPixelBuffer, SharedString, VecModel, Weak,
};

use crate::ui;

const TRANSPARENT_COLOR: Color = Color::from_argb_encoded(0x0000_0000);

fn solid_rgb_color(color: aoide::util::color::RgbColor) -> Color {
    Color::from_argb_encoded(0xff00_0000 | color.code())
}

#[must_use]
fn artwork_color(artwork: &Artwork) -> Option<Color> {
    let Artwork::Embedded(EmbeddedArtwork {
        image: ArtworkImage {
            color: Some(color), ..
        },
        ..
    }) = artwork
    else {
        return None;
    };
    Some(solid_rgb_color(*color))
}

type ArtworkThumbnailBuffer = SharedPixelBuffer<Rgb8Pixel>;

fn empty_artwork_thumbnail() -> ArtworkThumbnailBuffer {
    SharedPixelBuffer::new(0, 0)
}

#[must_use]
fn artwork_thumbnail(artwork: &Artwork) -> Option<ArtworkThumbnailBuffer> {
    let Artwork::Embedded(EmbeddedArtwork {
        image: ArtworkImage {
            thumbnail: Some(rgb_4x4),
            ..
        },
        ..
    }) = artwork
    else {
        return None;
    };
    Some(SharedPixelBuffer::clone_from_slice(rgb_4x4, 4, 4))
}

// Intermediate type that is only needed because slint::Image is not Send!
#[derive(Debug, Clone)]
struct SendableTrack {
    uid: SharedString,
    artist: SharedString,
    title: SharedString,
    color: Color,
    artwork_thumbnail: ArtworkThumbnailBuffer,
}

impl From<&track::repo_search::FetchedEntity> for SendableTrack {
    fn from(from: &track::repo_search::FetchedEntity) -> Self {
        let uid = from.entity.hdr.uid.to_string().into();
        let artist = from
            .entity
            .body
            .track
            .track_artist()
            .map_or_else(|| "Unknown Artist".into(), Into::into);
        let title = from
            .entity
            .body
            .track
            .track_title()
            .map_or_else(|| "Unknown Title".into(), Into::into);
        let color = from
            .entity
            .body
            .track
            .color
            .and_then(|color| match color {
                aoide::util::color::Color::Rgb(color) => Some(solid_rgb_color(color)),
                aoide::util::color::Color::Index(_) => None, // not supported
            })
            .or_else(||
                // Use the predominant artwork color as a fallback
                from
                .entity
                .body
                .track
                .media_source
                .artwork
                .as_ref()
                .and_then(artwork_color))
            .unwrap_or(TRANSPARENT_COLOR);
        let artwork_thumbnail = from
            .entity
            .body
            .track
            .media_source
            .artwork
            .as_ref()
            .and_then(artwork_thumbnail)
            .unwrap_or_else(empty_artwork_thumbnail);
        Self {
            uid,
            artist,
            title,
            color,
            artwork_thumbnail,
        }
    }
}

impl From<SendableTrack> for ui::Track {
    fn from(from: SendableTrack) -> Self {
        let SendableTrack {
            uid,
            artist,
            title,
            color,
            artwork_thumbnail,
        } = from;
        Self {
            uid,
            artist,
            title,
            color,
            artwork_thumbnail: Image::from_rgb8(artwork_thumbnail),
        }
    }
}

enum TrackListUpdate {
    Replace(Vec<SendableTrack>),
    Append(Vec<SendableTrack>),
}

#[must_use]
fn update_properties_in_event_loop(
    ui_handle: &Weak<ui::MainWindow>,
    state: &track::repo_search::State,
    memo: &mut track::repo_search::Memo,
) -> OnChanged {
    log::debug!("Updating track search properties");

    // Compose the updates outside of the UI event loop context before
    // applying them in the event loop. The UI event loop should be blocked
    // for the shortest possible time.
    let is_enabled = state.context().collection_uid.is_some();
    let accept_input = is_enabled && state.pending_since().is_none();
    let can_fetch_more = state.can_fetch_more().unwrap_or(false);
    debug_assert!(accept_input || !can_fetch_more);

    let append_offset = memo.fetch.fetched_entities.as_ref().map(|memo| memo.offset);
    let memo_diff = state.update_memo(memo);
    let track_list_update: Option<TrackListUpdate> = match memo_diff {
        track::repo_search::MemoDiff::Unchanged => None,
        track::repo_search::MemoDiff::Changed {
            fetched_entities: fetched_entities_diff,
        } => {
            let track_list_update = match fetched_entities_diff {
                track::repo_search::FetchedEntitiesDiff::Replace => {
                    let tracks = state
                        .fetched_entities()
                        .into_iter()
                        .flatten()
                        .map(Into::into)
                        .collect::<Vec<_>>();
                    log::debug!("Replacing {num_tracks} track(s)", num_tracks = tracks.len());
                    TrackListUpdate::Replace(tracks)
                }
                track::repo_search::FetchedEntitiesDiff::Append => {
                    let offset = append_offset.unwrap();
                    let tracks = state
                        .fetched_entities()
                        .into_iter()
                        .flatten()
                        .skip(offset)
                        .map(Into::into)
                        .collect::<Vec<_>>();
                    log::debug!(
                        "Appending {num_tracks} track(s) at offset {offset}",
                        num_tracks = tracks.len()
                    );
                    TrackListUpdate::Append(tracks)
                }
            };
            Some(track_list_update)
        }
    };

    ui_handle
        .upgrade_in_event_loop({
            move |ui| {
                ui.set_lib_track_search_is_enabled(is_enabled);
                ui.set_lib_track_search_accepts_input(accept_input);
                ui.set_lib_track_search_can_fetch_more(can_fetch_more);
                let model = ui.get_lib_track_search_list_model();
                let model = model.as_any().downcast_ref::<VecModel<ui::Track>>();
                // Unfortunately we have to transform the tracks again within the
                // event loop for converting `SendableTrack` into `ui::Track`. This
                // workaround is only required because `slint::Image` is not `Send`.
                match track_list_update {
                    None => (),
                    Some(TrackListUpdate::Append(tracks)) => {
                        let model = model.expect("Some");
                        log::debug!(
                            "Extending track search model: {old_row_count} -> {new_row_count}",
                            old_row_count = model.row_count(),
                            new_row_count = model.row_count() + tracks.len()
                        );
                        model.extend(tracks.into_iter().map(Into::into));
                    }
                    Some(TrackListUpdate::Replace(tracks)) => {
                        let tracks = tracks.into_iter().map(Into::into).collect::<Vec<_>>();
                        if let Some(model) = model {
                            log::debug!(
                                "Replacing track search model: {old_row_count} -> {new_row_count}",
                                old_row_count = model.row_count(),
                                new_row_count = tracks.len()
                            );
                            model.set_vec(tracks);
                        } else {
                            log::debug!(
                                "Initializing track search model: -> {row_count}",
                                row_count = tracks.len()
                            );
                            let model = ModelRc::new(VecModel::from(tracks));
                            ui.set_lib_track_search_list_model(model);
                        }
                    }
                }
            }
        })
        .map(|()| OnChanged::Continue)
        .unwrap_or(OnChanged::Abort)
}

pub fn on_state_changed_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    library: &Library,
) -> impl Future<Output = ()> + Send + 'static {
    let mut subscriber = library.state().track_search().subscribe_changed();
    let mut memo = Default::default();
    async move {
        log::debug!("Starting on_state_changed_property_updater");
        loop {
            match update_properties_in_event_loop(&ui_handle, &subscriber.read_ack(), &mut memo) {
                OnChanged::Continue => (),
                OnChanged::Abort => break,
            }
            log::debug!("Suspending on_state_changed_property_updater");
            if subscriber.changed().await.is_err() {
                break;
            }
            log::debug!("Resuming on_state_changed_property_updater");
        }
        log::debug!("Stopping on_state_changed_property_updater");
    }
}
